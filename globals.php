<?php

global $globals;

// index project dirctory
$globals['index'] = dirname(__FILE__);

 if ( !empty($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REQUEST_SCHEME'])) 
{
   $globals['site_url'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$globals['charset'] = 'UTF-8';
	$globals['asset_loc'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/asset';
	$globals['site_url_without_index'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
	$globals['events_path'] = $globals['site_url_without_index'].'/uploads/events';
	$globals['upload_path'] = $globals['site_url_without_index'].'/uploads';
	$globals['gallery_path'] = $globals['site_url_without_index'].'/gallery';
}

$globals['document_root'] = dirname(__FILE__);
$globals['user_image_upload_path'] = $globals['document_root'].'/uploads';

$globals['gallery_upload_path'] = $globals['document_root'].'/gallery';

$globals['max_min'] = 5;

//pagination
$globals['limit'] = 10;
$globals['video_per_page'] = 4;

//Blog Per Page
$globals['blog_per_page'] = 4;

// Gallery photos per page
$globals['gallery_per_page'] = 10;

// Event Listing Set it 1;
$globals['event_per_page'] = 2;


//Social link of stankskills
$globals['facebook'] = '#';
$globals['instagram'] = '#';
$globals['mail_id'] = 'vasu14082@gmail.com';
$globals['twitter'] = '#';
