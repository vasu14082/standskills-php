// test email
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

// var isadmin = "<?php echo (!empty(isadmin()) ? 1 : 0); ?>";
// var islogin = <?php echo (!empty(islogin()) ? json_encode(islogin()) : 0); ?>;

function error_hand_with_dismiss(error = []){
	var str = "";
	// str = '<div class="row" id="error">';
	$.each(error, function(key, value){
		str += '<div class="alert alert-danger alert-dismissible fade show col-sm-12" role="alert">'+value+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
	})
	// str += '</div>';
	return str;
}

function done_handler_with_dismiss_js(done = []){
	var str = "";
	// str = '<div class="row" id="error">';
	$.each(done, function(key, value){
		str += '<div class="alert alert-success alert-dismissible fade show col-sm-12" role="alert">'+value+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
	})
	// str += '</div>';
	return str;
}