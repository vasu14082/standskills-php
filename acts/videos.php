<?php

function videos(){

global $globals, $theme, $done, $error, $MYCONN, $addvideo, $videos, $page;

	$theme['init_theme'] = 'videos_theme';
	$theme['api_theme'] = 'API_videos';
	
	$addvideo = optREQ('addvideo', null);
	$page = optREQ('page', 0);

	if(optPOST('create_video')){

	}else{
		$videos = $MYCONN->get_videos($page);
	}

	// ss_print($videos);exit;

	if(optPOST('create_video')){
		// ss_print($_POST);
		$video_title = optPOST('video_title');
		$video_author = optPOST('video_author');
		$videos_link = optPOST('videos_link');
		$_user = islogin();
		if(empty($_user)){
			$error[] = 'Please login to create video';
			return false;
		}

		if(!isadmin()){
			$error[] = 'Only admin can create';
			return false;
		}

		if(empty($videos_link)){
			$error[] = 'Please enter youtube video link';
			return false;
		}

		if(!filter_var($videos_link, FILTER_VALIDATE_URL)){
			$error[] = 'Please enter valid url';
			return false;
		}

		$videos_link = get_youtube_id_from_url($videos_link);

		$return = $MYCONN->get_yt_video_by_link($videos_link);

		// var_dump($return);exit;

		if(!empty($return)){
			$error[] = 'Video is already available is our store by name'.$return['video_title'];
			return false;
		}

		$ret = $MYCONN->create_videos($_user['user_id'], $videos_link, $video_title, $video_author);

		if(empty($ret)){
			$error[] = 'Something went wrong while creating video';
			return false;
		}

		$done['msg'] = 'Video Created';
	}

}

function API_videos(){

global $globals, $theme, $done, $error, $MYCONN, $addvideo, $videos;

$API['done'] = $done;
$API['error'] = $error;
$API['videos'] = $videos;

return $API;
}