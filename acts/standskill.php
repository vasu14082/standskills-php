<?php
global $globals, $theme, $MYCONN;

define('STANDSKILLS', 'STANDSKILLS');

session_start();

include_once($globals['index'].'/main/connection.php');

// echo "<pre>";print_r(getenvs());exit;
$MYCONN = new MysqlConnect();
$MYCONN->create_database();
// echo '<pre>';var_dump($MYCONN->get_tables_list());exit;

$act = optGET('act');
$debug = optGET('debug');
$api = optGET('api');
if(!empty($debug)){
	function get_error(){
		var_dump(error_get_last());
		if($debug == 'died'){
			exit();
		}
	}
	register_shutdown_function('get_error');
}

switch($act){
	case 'shayri':
		include_once($globals['index'].'/modals/Database.php');
		include_once($globals['index'].'/modals/Shayri.php');
		include_once('shayri.php');
		shayri();
		break;
		
	case 'home':
		include_once('ssindex.php');
		ssindex();
		break;
	
	case 'login':
		include_once('login.php');
		login();
		break;

	case 'register':
		include_once('register.php');
		register();
		break;

	case 'logout':
		include_once('logout.php');
		logout();
		break;

	case 'addpost':
		include_once('addpost.php');
		addpost();
		break;

	case 'event':
		include_once('event.php');
		event();
		break;

	case 'verify':
		include_once('verify.php');
		verify();
		break;

	case 'blog':
		include_once('blog.php');
		blog();
		break;

	case 'videos':
		include_once('videos.php');
		videos();
		break;

	case 'gallery':
		include_once('gallery.php');
		gallery();
		break;

	case 'about':
		include_once('about.php');
		about();
		break;


	case 'contact':
		include_once('contact.php');
		contact();
		break;

	case 'payment':
		include_once('payment.php');
		payment();
		break;


	// case 'vijay':
	// 	include_once('vijay.php');
	// 	vijay();
	// 	break;
	default:
		include_once('ssindex.php');
		ssindex();
		break;
}

if($api == 'json'){

	if(!empty($theme['api_theme'])){
			echo json_encode(call_user_func($theme['api_theme']));
	}
	// echo json_encode($GLOBAL);
	return;
}
// var_dump($theme['init_theme']);exit;
if(!empty($theme['init_theme'])){
	if(file_exists($globals['index'].'/theme/'.$theme['init_theme'].'.php')){
		include_once($globals['index'].'/theme/'.$theme['init_theme'].'.php');
		call_user_func($theme['init_theme']);
	}
}
