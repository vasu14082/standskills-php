<?php
global $globals;

include_once($globals['document_root'].'/main/SendMail.php');

function login(){

global $globals, $theme, $MYCONN, $error, $done, $forgot, $forgot_conf;

	$theme['init_theme'] = 'login_theme';

	$forgot = optREQ('forgot');

	$forgot_conf = optREQ('cnfforgot');

	$redirect = urldecode(optREQ('redirect'));

	if(optPOST('login')){

		$email = optPOST('email');
		$password = optPOST('password');

		$ret = $MYCONN->do_login($email, $password);

		if(empty($ret)){
			return false;
		}
		$done['msg'] = 'Login Successfull';
		if(!empty($redirect)){
			$url = $redirect;
		}else{
			$url = $globals['site_url'].'?act=home';
		}
	// var_dump($url);exit();
		header('Location: '.html_entity_decode($url)); 
		exit();
	}

	if(optPOST('forgotpass') && !empty($forgot)){

		$email = optPOST('email');

		$ret = $MYCONN->get_user_by_email($email);
		// var_dump($ret);exit;

		if(empty($ret)){
			$MYCONN->close_conn();
			$error[] = 'User Not Found';
			return false;
		}

		$subject = 'Forget password for '.$email;
		$msg = 'Hi <b>'.$email.'</b><br>';
		$msg .= 'Click below link to forgot your password<br>';
		$msg .= $globals['site_url'].'?act=login&cnfforgot=pass&forgotemail='.$email.'&tfor=ss'.time();
		$msg .='<br>Regards,<br>';
		$msg .='<a href="'.$globals['site_url_without_index'].'"> standskills.in </a>';
		// var_dump($email, $subject, $msg);exit;
		$issend = ss_sendmail($email, $subject, $msg);
		if(empty($issend)){
			return false;
		}
		$done['msg'] = 'reset your password '.$email;
	}

	if(!empty($forgot_conf)){
		$timeforgot = (int)filter_var(optREQ('tfor'), FILTER_SANITIZE_NUMBER_INT);
		// $timenow = time();
		$diffminute = ceil((time() - $timeforgot)/60);
		// var_dump($globals);
		if($diffminute > $globals['max_min']){
			$error[] = 'Link is expired';
			return false;
		}

		$email = optREQ('forgotemail');

		// var_dump($email);exit;

		$ret = $MYCONN->get_user_by_email($email);
		// var_dump($ret);exit;

		if(empty($ret)){
			$MYCONN->close_conn();
			$error[] = 'User Not Found';
			return false;
		}

		$forgot_conf = array('email' => $email);
		
	}

	if(optREQ('forgot_conf')){

		$pass = optPOST('password');
		$cnf_pass = optPOST('cnf_password');
		$email = optPOST('email');
		if($pass != $cnf_pass){
			$error[] = 'The password is not matched';
			return false;
		}

		$ret = $MYCONN->get_user_by_email($email);
		// var_dump($ret);exit;

		if(empty($ret)){
			$MYCONN->close_conn();
			$error[] = 'User Not Found';
			return false;
		}

		$ret = $MYCONN->update_password_user($email, $pass);

		if(empty($ret)){
			$error[] = 'Something went wrong while updating password';
			return false;
		}

		
		$done['msg'] = 'Password is updated successfully';

	}

	return true;
}