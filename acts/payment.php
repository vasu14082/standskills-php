<?php

function payment(){

global $globals, $theme, $done, $error, $MYCONN, $data, $pay_details;

	$theme['init_theme'] = 'payment_theme';
	
	if(!empty(optGET('payment_id')) && !empty(optGET('payment_request_id'))){
		$data = [];
		$data['payment_id'] = optGET('payment_id');
		$data['status'] = optGET('payment_status');

		$ret = $MYCONN->update_payment($data, optGET('payment_request_id'));

		$paydata = [];
		$paydata['payment_id'] = optGET('payment_id');
		$paydata['payment_status'] = optGET('payment_status');
		// $paydata['payment_type_id'] = optGET('payment_type_id');

		$ret = $MYCONN->update_payment_details($paydata, optGET('payment_request_id'));
		$pay_details = $MYCONN->get_payment_detail(optGET('payment_request_id'));
		// var_dump($pay_details);
	}

	if(!empty(optREQ('webhook'))){
		$_GET['webhook'] = '';
		makelog($_GET, $_POST);	
	}
}