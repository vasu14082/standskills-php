<?php

function blog(){
	global $globals, $theme, $MYCONN, $error, $done, $_user, $addblog, $blogs, $_blog, $_blog_like, $_blog_comment;

	$theme['init_theme'] = 'blog_theme';
	$theme['api_theme'] = 'API_blog';

	$_user = islogin();
	$page = 0;
	$blogs = [];
	if(!empty(optGET('addblog', NULL))){
		
		$addblog = optGET('addblog', NULL);
	}elseif(!empty(optGET('blog', NULL))){
		$blog = optGET('blog', NULL);
	}else{
		$page = (int)optREQ('page', 0);
		$blogs = $MYCONN->get_blogs($page);
	}
	// echo '<pre>'; print_r($blogs);exit;
	// $blog_likes = $MYCONN->get_blog_likes_by_user($_user['user_id']);

	
	$_blog = [];
	if(!empty($blog)){
		if(array_key_exists($blog, $blogs)){
			$_blog = $blogs[$blog];
		}else{
			$_blog = $MYCONN->get_blog_by_id($blog);
		}
		if(empty($_blog)){
			$error[] = 'Blog not found';
			return false;
		}

		$page = (int)optREQ('page', 0);
		$_blog['comments'] = $MYCONN->get_blog_comment_by_blog_id($_blog['blog_id'], $page);
		// ss_print($_blog);exit;	
	}

	// ss_print($_blog);

	// $event_notifications = $MYCONN->get_notification_from_user($_user['user_id']);
	// echo '<pre>';var_dump($event_notifications);exit;
	// $blog_like = optREQ('blog_like', null);

	// var_dump($_POST);exit;
	if(!empty(optREQ('blog_like'))){
		// print_r($_POST);print_r($blog);exit;
		if(empty($_user['user_id'])){
			$error[] = 'please login to like this post';
			return false;
		}
		$blog_id = optPOST('blog_id');
		$is_like = optPOST('is_like');

		$_blog_like = $MYCONN->get_blog_like($_user['user_id'], $blog_id);

		// var_dump($return);exit;
		if(!empty($_blog_like)){
			$ret = $MYCONN->update_blog_like($_blog_like['blog_like_id'], (bool)$is_like);
		}else{
			$ret = $MYCONN->create_blog_like($_user['user_id'], $blog_id, $is_like);
		}

		$_blog_like = $MYCONN->get_blog_like($_user['user_id'], $blog_id);
		$_blog_like['likes_count'] = $MYCONN->get_blog_likes_count($blog_id);
		// var_dump($_user['user_id'], $blog_id, $is_like);exit;
		// var_dump($ret);var_dump($error);exit;
		if(empty($ret)){
			return false;
		}

		$done['msg'] = 'You have '.(!empty($_blog_like['is_like']) ? '' : 'un').'like this blog';
	}

	if(!empty(optREQ('blog_comment'))){

		$blog_id = optPOST('blog_id');
		$comment_text = optPOST('blog_comment_text');
		// ss_dump($blog_id);ss_dump($comment_text);exit;

		if(empty($_user)){
			$error[] = 'please login to comment on this post';
			return false;
		}

		$ret = $MYCONN->create_blog_comment($_user['user_id'], $blog_id, $comment_text, true);

		if(empty($ret)){
			$error[] = 'Something Went wrong';
			return false;
		}

		$_blog_comment = $ret;
		$_blog_comment['total_comment'] = $MYCONN->get_blog_comments_count($blog_id);

		$done['msg'] = 'You have commentted on this blog';
	}

	if(isset($_POST['create_blog'])){
		// echo '<pre>';var_dump($_POST);
		// var_dump($_FILES);exit;
		if(isset($_FILES["blog_image"]["name"]) && !empty($_FILES["blog_image"]["name"])){
			$tempname = $_FILES["blog_image"]["tmp_name"];
			$filename = $_FILES["blog_image"]["name"];

			if(!is_dir($globals['user_image_upload_path'])){
				mkdir($globals['user_image_upload_path'], 755, true);
			}
			
			$folder = $globals['user_image_upload_path'].'/blogs';
			$ext = pathinfo($filename, PATHINFO_EXTENSION);

			if(!is_dir($folder)){
				mkdir($folder, 755, true);
			}

			$post_img = $_user['user_id'].'_'.time().'.'.$ext;
			$folder = $folder.'/'.$post_img;

			// Now let's move the uploaded image into the folder: image
	        if (!move_uploaded_file($tempname, $folder)){
	            $error[] = 'Failed to upload image';
	            return false;
	        }
		}

        $blog = optPOST('blog');
        $blog_title = optPOST('blog_title');

        if(!empty($post_img)){
        	$ret = $MYCONN->create_blog($_user['user_id'], $blog_title, $blog, $post_img);
        }else{
        	$ret = $MYCONN->create_blog($_user['user_id'], $blog_title, $blog);
        }

        if(empty($ret)){
        	$error[] = 'Something went wrong while creating blog';
        	return false;
        }
        
        $done['msg'] = 'Blog Created';
	}

	return true;
}

function API_blog(){
	global $globals, $theme, $MYCONN, $error, $done, $_user, $addblog, $blogs, $_blog, $_blog_like, $_blog_comment;

	$API['done'] = $done;
	$API['error'] = $error;
	$API['blogs'] = $blogs;
	$API['_blog'] = $_blog;
	$API['_user'] = $_user;
	$API['_blog_like'] = $_blog_like;
	$API['_blog_comment'] = $_blog_comment;

	return $API;
}