<?php

function addpost(){
	global $globals, $theme, $MYCONN, $error, $done;

	$theme['init_theme'] = 'addpost_theme';

	$_user = $MYCONN->get_user_details();
	if(empty($_user)){
		$url = urlencode($globals['site_url'].'?act=login&redirect=addpost');
		header('Location: '.$globals['site_url'].'?act=login&redirect=addpost'); 
	}

	if(isset($_POST['create_shayri'])){
		$tempname = $_FILES["shayri_image"]["tmp_name"];
		$filename = $_FILES["shayri_image"]["name"];  
		$folder = $globals['user_image_upload_path'].'/posts/'.$_user['user_id'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(!is_dir($folder)){
			mkdir($folder, 755, true);
		}

		$post_img = $_user['user_id'].'_'.time().'.'.$ext;
		$folder = $folder.'/'.$post_img;
		
		// Now let's move the uploaded image into the folder: image
        if (!move_uploaded_file($tempname, $folder)){
            $error[] = 'Failed to upload image';
            return false;
        }

        $shayri = optPOST('shayri');

        $ret = $MYCONN->create_post($_user['user_id'], $shayri, $post_img);
        // var_dump($ret);exit;
		if(empty($ret)){
			$error[] = 'Something went wrong';
			return false;
		}

		$done['msg'] = 'Post created';
	}

	return true;
}