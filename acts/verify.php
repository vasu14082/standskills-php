<?php
global $globals;

function verify(){

global $globals, $theme, $MYCONN, $error, $done;

	$theme['init_theme'] = 'verify_theme';

	$user_id = optGET('user_id', null);

	if(empty($user_id)){
		$error[] = 'User doesn\'t exist in our database please contact to standskills.in';
		return false;
	}

	$ret = $MYCONN->verify_user($user_id);

	if(empty($ret)){
		return false;
	}
		
	$done['msg'] = 'Please Click link to verify which is send on your given email';

	return true;
	
}