<?php

include_once $globals['index'].'/main/instamojopay.php';

function event(){
	global $globals, $theme, $MYCONN, $error, $done, $addevent, $events, $event_notifications, $_event, $event, $register, $showtickets, $tickets, $notify_email_list, $show;

	$theme['init_theme'] = 'event_theme';
	$theme['api_theme'] = 'API_event';
	$_user = islogin();

	/*$data = [
		"success" => true,
	    "payment_requests" => [
	        "id"=> "d66cb29dd059482e8072999f995c4eef",
	        "phone"=> "+919999999999",
	        "email"=> "foo@example.com",
	        "buyer_name"=> "John Doe",
	        "amount"=> "2500",
	        "purpose"=> "FIFA 16",
	        "status"=> "Pending",
	        "send_sms"=> true,
	        "send_email"=> true,
	        "sms_status"=> "Pending",
	        "email_status"=> "Pending",
	        "shorturl"=> null,
	        "longurl"=> "https://www.instamojo.com/@ashwch/d66cb29dd059482e8072999f995c4eef/",
	        "redirect_url"=> "http://www.example.com/redirect/",
	        "webhook"=> "http://www.example.com/webhook/",
	        "created_at"=> "2015-10-07T21:36:34.665Z",
	        "modified_at"=> "2015-10-07T21:36:34.665Z",
	        "allow_repeated_payments"=> false
		    ]
	];
	$query = 'INSERT INTO payment('.implode(', ', array_keys($data["payment_requests"])).') VALUES ('.implode(', ', array_values($data["payment_requests"])).')';*/

	// ss_dump(create_payment_request());
	if(isset($_POST['new_payment_form_submit'])){

		if(empty($_user)){
			$error[] = 'Please log to to register into this event';
			return false;
		}

		if(optPOST('csrf_token') != gettoken()){
			$error[] = 'CSRF token mitchmatch';
			return false;
		}

		// ss_dump($_POST, json_encode($_POST['bill']));exit;
		// makelog($_POST);
		$ret = handle_payment();
		if(empty($ret)){
			return false;
		}
	}
	
	// ss_dump(getenvs());
	// ss_dump($cat_events);exit;

	$addevent = optGET('addevent', null);
	$register = optGET('register');
	$page = optREQ('page', 0);
	$showtickets = optGET('showtickets');
	$show = optGET('show');
	if(!empty(optREQ('notify'))){

	}elseif(!empty($register)){
		$event_id = optGET('event_id', null);
		$event = $MYCONN->get_event_by_id($event_id);

	}elseif(!empty($showtickets)){

		$event_id = optGET('event_id', null);
		$event = $MYCONN->get_event_by_id($event_id);

		if(empty($_user)){
			$url = $globals['site_url'].'?act=event&showtickets=1&event_id='.$event_id;
			$error[] = 'Please login to see your ticket history <a href="'.$globals['site_url'].'?act=login&redirect='.urlencode($url).'">Click Here</a>';
			return false;
		}

		$tickets = $MYCONN->get_payment_details($_user['user_id']);

	}elseif(!empty($show)){
		$event_id = optGET('event_id', null);
		$notify_email_list = $MYCONN->get_event_notfication($event_id);
		// ss_print($notify_email_list);exit;
	}else{

		$events = $MYCONN->get_events($page);
		// ss_print($events);
	}

	if(optREQ('notify')){

		$isnotify = (int)optREQ('is_notify', false);
		$event_id = optREQ('event_id', false);
		$email = optREQ('email');
		// var_dump($isnotify, $event_id);exit;
		if(empty($email)){
			$error[] = 'Please Enter email';
			return false;
		}

		$ret = $MYCONN->get_event_by_id($event_id);
		// var_dump($ret);exit;
		if(empty($ret)){
			$error[] = 'Event not found';
			return false;
		}

		$_notify = $MYCONN->get_notification_from_email($event_id, $email);

		if(!empty($_notify)){
			$error[] = 'Email already exists we will notify on this email';
			return false;
		}

		$ret = $MYCONN->create_event_notification($event_id, $email, $isnotify);

		if(empty($ret)){
			$error[] = 'Something Went wrong';
			return false;
		}

		$_event = $MYCONN->get_notification_from_email($event_id, $email);
		
		$done['msg'] = 'We will notify on given Email';
	
	}

	if(isset($_POST['create_event'])){
		if(empty(isadmin())){
			$error[] = 'Please login as as Admin to create Event';
			return false;
		}
		// ss_print($_POST);exit;
		$tempname = $_FILES["event_image"]["tmp_name"];
		$filename = $_FILES["event_image"]["name"];  
		$folder = $globals['user_image_upload_path'].'/events';
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(!is_dir($folder)){
			mkdir($folder, 755, true);
		}

		$post_img = $_user['user_id'].'_'.time().'.'.$ext;
		$folder = $folder.'/'.$post_img;
		
		// // Now let's move the uploaded image into the folder: image
  //       if (!move_uploaded_file($tempname, $folder)){
  //           $error[] = 'Failed to upload image';
  //           return false;
  //       }

        $event_name = optPOST('event_name');
        $event_add = optPOST('event_add');
        $event_date = optPOST('event_date');
        $event_time = optPOST('event_time');
        $event_details = optPOST('event_details');
        $event_price = json_encode($_POST['price']);
        // ss_print($event_price);exit;
        if(empty($tempname)){
        	$error[] = 'Event image is required';
        }

        if(empty($event_name)){
        	$error[] = 'Event Name is required';
        }

        if(empty($event_add)){
        	$error[] = 'Event Address is required';
        }

        if(empty($event_date)){
        	$error[] = 'Event Date is required';
        }

        if(empty($event_time)){
        	$error[] = 'Event Time is required';
        }

        if(empty($event_price)){
        	$error[] = 'Price is not Set';
        }

        // Check if any error
        if(!empty($error)){
        	return false;
        }
        // Now let's move the uploaded image into the folder: image
        if (!move_uploaded_file($tempname, $folder)){
            $error[] = 'Failed to upload image';
            return false;
        }

        // Check if any error
        if(!empty($error)){
        	return false;
        }

        // echo "<pre>";var_dump($event_name, $event_add, $event_date, $event_time, $event_details, $post_img);exit;
        $ret = $MYCONN->create_event($_user['user_id'], $event_name, $event_add, $event_details, $event_date, $event_time, $post_img, $event_price);
        // var_dump($ret);exit;
		if(empty($ret)){
			$error[] = 'Something went wrong';
			return false;
		}

		$done['msg'] = 'Event Created';
	}

	return true;
}

function API_event(){
global $globals, $theme, $MYCONN, $error, $done, $addevent, $events, $event_notifications, $_event, $API, $event;
$API['done'] = $done;
$API['error'] = $error;
$API['events'] = $events;
$API['_event'] = $_event;
$API['event'] = $event;

return $API;
}

function handle_payment(){
	global $globals, $theme, $MYCONN, $error, $done;

	$event_id = optGET('event_id', null);
	$event = $MYCONN->get_event_by_id($event_id);
	if(empty($event)){
		$error[] = 'Something went wrong or event not found';
		return false;
	}
	$_user = islogin();

	$data = Array(
	    'purpose' => optPOST('payment_type').' for '.$event['event_name'],
	    'amount' => optPOST('final_event_price'),
	    'phone' => $_user['mobile_no'],
	    'buyer_name' => $_user['user_name'],
	    'redirect_url' => $globals['site_url'].'?act=payment&payment_type='.optPOST('payment_type').'&payment_type_id='.$event_id,
	    'webhook' => $globals['site_url'].'?act=payment&webhook='.$event_id,
	    'email' => $_user['email'],
	    'allow_repeax`ted_payments' => false
	);

	$post_data = [
		'payment_amount' => optPOST('final_event_price'),
		'payment_type' => optPOST('payment_type'),
		'payment_fields' => html_entity_decode(optPOST('event_price_fields')),
		'payment_bill_fields' => json_encode($_POST['bill']),
		'payment_type_id' => $event_id
	];
	// var_dump(html_entity_decode($a), true));exit;
	// ss_dump($post_data);exit;
	$ret = create_payment_request($data, $post_data);
	if(empty($ret)){
		return false;
	}
	return true;
}