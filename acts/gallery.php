<?php

function gallery(){

	global $globals, $theme, $done, $error, $MYCONN, $addtogallery, $gallery, $_gallery;

	$theme['init_theme'] = 'gallery_theme';
	$theme['api_theme'] = 'API_gallery';

	$addtogallery = optREQ('addtogallery');

	$page = optREQ('page', 0);
	$set_on_home = optREQ('set_on_home', null);
	$delete_gallery = optREQ('delete_gallery', null);

	if(!empty($addtogallery)){

	}elseif(!empty($set_on_home)){

	}elseif(!empty($delete_gallery)){

	}else{
		$gallery = $MYCONN->get_gallery($page);
	}
	
	// ss_print($gallery);exit;

	if(isset($_POST['create_gallary'])){
		$tempname = $_FILES["gallery_image"]["tmp_name"];
		$filename = $_FILES["gallery_image"]["name"];  
		$folder = $globals['gallery_upload_path'];

		if (!is_uploaded_file($tempname)){
            $error[] = 'Please upload gallery Image';
            return false;
        }

        $_user = islogin();

        if(empty($_user)){
        	 $error[] = 'Please loging to upload gallery';
            return false;
        }

		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(!is_dir($folder)){
			mkdir($folder, 755, true);
		}

		$post_img = time().'.'.$ext;
		$folder = $folder.'/'.$post_img;
		
		// Now let's move the uploaded image into the folder: image
        if (!move_uploaded_file($tempname, $folder)){
            $error[] = 'Failed to upload image';
            return false;
        }

        $insta_id = optPOST('gallery_instaid');

        $ret = $MYCONN->create_gallery($_user['user_id'], $post_img, $insta_id);
        // var_dump($ret);exit;
		if(empty($ret)){
			$error[] = 'Something went wrong';
			return false;
		}

		$done['msg'] = 'Gallery Created';
	}

	// Set image on home
	if(!empty($set_on_home)){
		$gallery_id = optPOST('gallery_id');
		$ishome = optPOST('ishome');

		// var_dump($gallery_id, $ishome);exit;
		$ret = $MYCONN->get_gallery_by_id($gallery_id);

		if(empty($ret)){
			$error[] = 'Photo doesn\'t exist';
			return false;
		}

		$ret = $MYCONN->set_gallery_as_home($gallery_id, $ishome);

		if(empty($ret)){
			$error[] = 'Something went wrong';
			return false;
		}

		$_gallery = $MYCONN->get_gallery_by_id($gallery_id);

		$done['msg'] = 'Set as this gallery to home';
	}

	if(!empty($delete_gallery)){
		$gallery_id = optPOST('gallery_id');
		$gallery_name = optPOST('gallery_name');
		// var_dump($gallery_name, $gallery_id);exit;
		$ret = $MYCONN->get_gallery_by_id($gallery_id);

		if(empty($ret)){
			$error[] = 'Photo doesn\'t exist';
			return false;
		}

		$ret = $MYCONN->delete_gallery_by_id($gallery_id);
		if(empty($ret)){
			$error[] = 'Something Went wrong';
			return false;
		}

		unlink($globals['gallery_path'].'/'.$gallery_name);

		$done['msg'] = 'Gallery deleted';
	}
	// ss_dump($gallery);exit;
}

function API_gallery(){
	global $globals, $theme, $done, $error, $MYCONN, $addtogallery, $gallery, $_gallery;
	$API['done'] = $done;
	$API['error'] = $error;
	$API['_gallery'] = $_gallery;
	$API['gallery'] = $gallery;
	return $API;
}