<?php

$migrations = [
	'shayri_table' =>[
		'up' => [
			'action' => 'create',
			'table_name' => 'shayri',
			'schema' => [
				'shayri_id' => 'INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
				'user_id' => 'INT(10) UNSIGNED',
				'shayri' => 'VARCHAR(100) NOT NULL DEFAULT ""',
				'shayari_image' => 'VARCHAR(100) NOT NULL DEFAULT ""'
			],
			'ref' => [
				'user_id' => [
					'table' => 'users',
					'table_field' => 'user_id'
				],
			],
		],
		'down' => [
			'query' => 'DROP TABLE shayri',
		]
	]
];