<?php
// used to get mysql database connection
class Database{
 
    // specify your own database credentials
    private $driver;
    private $host;
    private $port;
    private $db_name;
    private $username;
    private $password;
    public $conn;

    public function __construct(){
        $this->driver = env('DB_DRIVER', 'mysql');
        $this->host = env('DB_HOST', 'localhost');
        $this->port = env('DB_PORT', '3306');
        $this->db_name = env('DB_NAME', 'standskills');
        $this->username = env('DB_USER', 'root');
        $this->password = env('DB_PASS', '');
    }
 
    // get the database connection
    public function getConnection(){
    global $globals, $error;
        $this->conn = null;
 
        try{
            $this->conn = new PDO($this->driver.":host=" . $this->host.";dbname=" . $this->db_name, $this->username, $this->password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // $sql = 'CREATE DATABASE IF NOT EXISTS '.$this->db_name;
            // use exec() because no results are returned
            // $conn->exec($sql);
        }catch(PDOException $exception){
            $error['db_connection_error'] = "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}