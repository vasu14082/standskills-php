<?php
/**shayari modal
 * 
 */
class Shayri
{
	public $conn;

	protected $table_name = 'shayri';

	public $column_fields = [
		'shayri_id' => '',
		'user_id' => '',
		'shayri' => '',
		'shayari_image' => '',
	];

	public $struct = [
		'shayri_id' => 'INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY',
		'user_id' => 'INT(10) UNSIGNED',
		'shayri' => 'VARCHAR(100) NOT NULL DEFAULT ""',
		'shayari_image' => 'VARCHAR(100) NOT NULL DEFAULT ""'
	];

	public $ref = [
		'user_id' => [
			'table' => 'users',
			'table_field' => 'user_id'
		]
	];

	protected $timestamp = true;

	public function __construct($conn)
	{
		$this->conn = $conn;
	}
	
	public function create($data = []){

	global $globals, $error;
		
		try{
			// query to insert record
	    	$query = 'INSERT INTO' . $this->table_name . 'SET user_id=:user_id, shayri=:shayri, shayari_image=:shayari_image';

	    	// prepare query
	    	$stmt = $this->conn->prepare($query);
	    	// sanitize
	    	$this->column_fields['user_id'] = htmlspecialchars(strip_tags($this->column_fields['user_id']));
	    	$this->column_fields['shayri'] = htmlspecialchars(strip_tags($this->column_fields['shayri']));
	    	$this->column_fields['shayari_image'] = htmlspecialchars(strip_tags($this->column_fields['shayari_image']));

	    	// bind values
	    	$stmt->bindParam(":user_id", $this->column_fields['user_id']);
	    	$stmt->bindParam(":shayri", $this->column_fields['shayri']);
	    	$stmt->bindParam(":shayari_image", $this->column_fields['shayari_image']);
	    	// execute query
		    if($stmt->execute()){
		        return true;
		    }
		  
		    return false;
    	}catch(Exception $e){
    		$error['table_create_error'] = 'Error while creating table : '.$e->getMessage();
    		return false;
    	}

	}

	// read Shayri
	function read(){ 
	global $globals, $error, $done;

		$response = [];

		try{
			 // select all query
		    $query = "SELECT * FROM ".$this->table_name;
		  
		    // prepare query statement
		    $stmt = $this->conn->prepare($query);
		  
		    // execute query
		    $stmt->execute();
		    $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    $response['data'] = $stmt->fetchAll();
		    $response['total'] = $this->count();
			$response['page'] = 0;
			$response['fetched'] = $stmt->rowCount();

			return $response;

		}catch(Exception $e){
    		$error['fetch_error'] = 'Error while fetching data : '.$e->getMessage();
    	}
	 	
	 	return $response;
	}

	// used for paging products
	public function count(){
	global $globals, $error, $done;

		try{
			$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		  
		    $stmt = $this->conn->prepare( $query );
		    $stmt->execute();
		    $row = $stmt->fetch(PDO::FETCH_ASSOC);
		  
		    return $row['total_rows'];
		}catch(Exception $e){
    		$error['count_error'] = 'Error while counting data : '.$e->getMessage();
    	}
    	return 0;
	}
}