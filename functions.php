<?php

function ss_header($title = 'StandSkills', $meta=''){
	echo '
</<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>'.$title.'</title>';
  if(!empty($meta)){
    echo $meta;
  }
  echo '
	<link rel="stylesheet" href="./asset/bootstrap/bootstrap.min.css" />
	<link rel="stylesheet" href="./asset/fontawesome/all.css" />
  <link rel="stylesheet" href="./asset/webcss/common.css" />
  <link href="https://fonts.googleapis.com/css2?family=Reenie+Beanie&display=swap" rel="stylesheet">
	<script src="./asset/bootstrap/jquery.min.js"></script>
	<script src="./asset/bootstrap/jquery-3.1.1.min.js"></script>
	<!-- <script src="./asset/bootstrap/jquery-3.2.1.slim.min.js"></script> -->
	<script src="./asset/bootstrap/popper.min.js"></script>
	<script src="./asset/bootstrap/bootstrap.min.js"></script>
	<script src="./asset/webjs/common.js"></script>
  <script>
    var isadmin = '.(!empty(isadmin()) ? 1 : 0).'
    var islogin = '.(!empty(islogin()) ? json_encode(islogin()) : 0).'
  </script>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			
		</div>
	</div>

';

}

function ss_navigation($act = 'home'){

global $globals;

	echo '

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #f50202;">
	<a class="navbar-brand" href="'.$globals['site_url'].'?act=home"><img src="'.$globals['asset_loc'].'/img/standskill.jpeg" height="60" width="60" alt="standskill" class="rounded"></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link '.($act == 'home' || $act == '' ? 'active' : '').'" href="'.$globals['site_url'].'">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link '.($act == 'shayri' ? 'active' : '').'" href="'.$globals['site_url'].'?act=shayri">Shayri & poem</a>
      </li>
      <li class="nav-item">
	    <a class="nav-link '.($act == 'event' ? 'active' : '').'" href="'.$globals['site_url'].'?act=event">Upcoming Events</a>
	  </li>

	  <li class="nav-item">
	    <a class="nav-link '.($act == 'blog' ? 'active' : '').'"  href="'.$globals['site_url'].'?act=blog">Create blog or Script</a>
	  </li>

	  <li class="nav-item">
	    <a class="nav-link '.($act == 'gallery' ? 'active' : '').'"  href="'.$globals['site_url'].'?act=gallery">Gallery</a>
	  </li>

	  <li class="nav-item">
	    <a class="nav-link '.($act == 'videos' ? 'active' : '').'"  href="'.$globals['site_url'].'?act=videos">Videos</a>
	  </li>

	  <li class="nav-item">
      <a class="nav-link '.($act == 'about' ? 'active' : '').'"  href="'.$globals['site_url'].'?act=about">About Us</a>
    </li>

	  <li class="nav-item">
      <a class="nav-link '.($act == 'contact' ? 'active' : '').'"  href="'.$globals['site_url'].'?act=contact">contact us</a>
    </li>

  </ul> 
    <ul class="navbar-nav my-2 my-lg-0">';

if(!empty($_SESSION['user_details'])){
	
	echo '
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle '.($act == 'addpost' || $act == 'profile' ? 'active' : '').'" href="'.$globals['site_url'].'" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="'.($_SESSION['user_details']['role'] != 'admin' ? 'far fa-user' : 'fas fa-user-shield').'">
	  		'.$_SESSION['user_details']['user_name'].'</i>
		</a>
		<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		  <a class="dropdown-item" href="'.$globals['site_url'].'?act=profile">Profile</a>
		  <a class="dropdown-item" href="'.$globals['site_url'].'?act=user_details">add post</a>
		  <a class="dropdown-item" href="'.$globals['site_url'].'?act=logout">Logout</a>
		</div>
	</li>';

}else{

	echo '<li class="nav-item">
        <a class="nav-link" href="'.$globals['site_url'].'?act=login">Login</a>
      </li>';
}

echo '</ul>
  </div>
</nav>';
}
function ss_footer(){
global $globals;
echo '
<style>
.site-footer
{
  background-color:#26272b;
  padding:45px 0 20px;`
  font-size:15px;
  line-height:24px;
  color:#737373;
  position: relative;
  z-index: 999;
}
.site-footer hr
{
  border-top-color:#bbb;
  opacity:0.5
}
.site-footer hr.small
{
  margin:20px 0
}
.site-footer h6
{
  color:#fff;
  font-size:16px;
  text-transform:uppercase;
  margin-top:5px;
  letter-spacing:2px
}
.site-footer a
{
  color:#737373;
}
.site-footer a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links
{
  padding-left:0;
  list-style:none
}
.footer-links li
{
  display:block
}
.footer-links a
{
  color:#737373
}
.footer-links a:active,.footer-links a:focus,.footer-links a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links.inline li
{
  display:inline-block
}
.site-footer .social-icons
{
  text-align:right
}
.site-footer .social-icons a
{
  width:40px;
  height:40px;
  line-height:40px;
  margin-left:6px;
  margin-right:0;
  border-radius:100%;
  background-color:#33353d
}
.copyright-text
{
  margin:0
}
@media (max-width:991px)
{
  .site-footer [class^=col-]
  {
    margin-bottom:30px
  }
}
@media (max-width:767px)
{
  .site-footer
  {
    padding-bottom:0
  }
  .site-footer .copyright-text,.site-footer .social-icons
  {
    text-align:center
  }
}
.social-icons
{
  padding-left:0;
  margin-bottom:0;
  list-style:none
}
.social-icons li
{
  display:inline-block;
  margin-bottom:4px
}
.social-icons li.title
{
  margin-right:15px;
  text-transform:uppercase;
  color:#96a2b2;
  font-weight:700;
  font-size:13px
}
.social-icons a{
  background-color:#eceeef;
  color:#818a91;
  font-size:16px;
  display:inline-block;
  line-height:44px;
  width:44px;
  height:44px;
  text-align:center;
  margin-right:8px;
  border-radius:100%;
  -webkit-transition:all .2s linear;
  -o-transition:all .2s linear;
  transition:all .2s linear
}
.social-icons a:active,.social-icons a:focus,.social-icons a:hover
{
  color:#fff;
  background-color:#29aafe
}
.social-icons.size-sm a
{
  line-height:34px;
  height:34px;
  width:34px;
  font-size:14px
}
.social-icons a.facebook:hover
{
  background-color:#3b5998
}
.social-icons a.twitter:hover
{
  background-color:#00aced
}
.social-icons a.linkedin:hover
{
  background-color:#cd486b
}
.social-icons a.dribbble:hover
{
  background-color:#ea4c89
}
@media (max-width:767px)
{
  .social-icons li.title
  {
    display:block;
    margin-right:0;
    font-weight:600
  }
}
</style>
';

	echo '
  <!-- Site footer -->
</div>
  <footer class="site-footer fixed-bottom">
	  <div class="container">
	    <div class="row">
	      <div class="col-sm-12 col-md-6">
	        <h6>About</h6>
	        <p class="text-justify">StandSkills <i> </i> "Everyone is an artist but very few get a platform to perform"
Standskill will give a unique opportunity to perform and share your thoughts, skills and emotion through the power of poetry, storytelling, singing, and let\'s laugh together with standup comedy\'.

We will be happy to see you on the stage..</p>
	      </div>

	     <!--
	      <div class="col-xs-6 col-md-3">
	        <h6>Categories</h6>
	        <ul class="footer-links">
	          <li><a href="http://scanfcode.com/category/c-language/">C</a></li>
	          <li><a href="http://scanfcode.com/category/front-end-development/">UI Design</a></li>
	          <li><a href="http://scanfcode.com/category/back-end-development/">PHP</a></li>
	          <li><a href="http://scanfcode.com/category/java-programming-language/">Java</a></li>
	          <li><a href="http://scanfcode.com/category/android/">Android</a></li>
	          <li><a href="http://scanfcode.com/category/templates/">Templates</a></li>
	        </ul>
	      </div>
	      -->

	      <div class="col-xs-6 col-md-3">
	        <h6>Quick Links</h6>
	        <ul class="footer-links">
	          <li><a href="'.$globals['site_url'].'?act=about">About Us</a></li>
	          <li><a href="'.$globals['site_url'].'?act=contact">Contact Us</a></li>
	        </ul>
	      </div>
	    </div>
	    <hr>
	  </div> 
	  <div class="container">
	    <div class="row">
	      <div class="col-md-8 col-sm-6 col-xs-12">
	        <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
	     <a href="'.$globals['site_url_without_index'].'">StandSkills</a>.
	        </p>
	      </div>

	      <div class="col-md-4 col-sm-6 col-xs-12">
	        <ul class="social-icons">
	          <li><a class="facebook" href="'.$globals['facebook'].'"><i class="fab fa-facebook fa-2x"></i></a></li>
	          <li><a class="twitter" href="'.$globals['twitter'].'"><i class="fab fa-twitter-square fa-2x"></i></a></li>
	          <li><a class="dribbble" target="_blank" href="mailto:'.$globals['mail_id'].'"><i class="far fa-envelope fa-2x"></i></a></li>
	          <li><a class="linkedin" href="'.$globals['instagram'].'"><i class="fab fa-instagram fa-2x"></i></a></li>   
	        </ul>
	      </div>
	    </div>
	  </div>
</footer>
</body>
</html>
';

// 	echo '
// 	</div>
// 	<div class="fixed-bottom"><center>&copy; StandSkills.in</center></div>
// 	</body>
// </html>
// ';
}


function optREQ($name, $default = ''){

	//Check the POSTED NAME was posted
	if(isset($_REQUEST[$name])){
	
		return inputsec(htmlizer(trim($_REQUEST[$name])));
		
	}else{
		
		return $default;
	
	}

}

function optGET($name, $default = ''){
	
	//Check the GETED NAME was GETed
	if(isset($_GET[$name])){
	
		return inputsec(htmlizer(trim($_GET[$name])));
		
	}else{
		
		return $default;
	
	}

}

function optPOST($name, $default = ''){

	//Check the POSTED NAME was posted
	if(isset($_POST[$name])){
	
		return inputsec(htmlizer(trim($_POST[$name])));
		
	}else{
		
		return $default;
	
	}

}

function inputsec($string){
	
	//get_magic_quotes_gpc is depricated in php 7.4
	if(version_compare(PHP_VERSION, '7.4', '<')){
		if(!get_magic_quotes_gpc()){
		
			$string = addslashes($string);
		
		}else{
		
			$string = stripslashes($string);
			$string = addslashes($string);
		
		}
	}else{
		$string = addslashes($string);
	}
	
	// This is to replace ` which can cause the command to be executed in exec()
	$string = str_replace('`', '\`', $string);
	
	return $string;

}

function htmlizer($string){

global $globals;

	$string = htmlentities($string, ENT_QUOTES, $globals['charset']);
	
	preg_match_all('/(&amp;#(\d{1,7}|x[0-9a-fA-F]{1,6});)/', $string, $matches);//r_print($matches);
	
	foreach($matches[1] as $mk => $mv){
		$tmp_m = entity_check($matches[2][$mk]);
		$string = str_replace($matches[1][$mk], $tmp_m, $string);
	}
	
	return $string;
	
}

function get_safe_string($string){
	return inputsec(htmlizer(trim($string)));
}


function error_handle($error = []){
	if(empty($error)){
		return false;
	}
	$str = '';
	foreach ($error as $key => $value) {
		$str .= '<li>'.$value.'</li>';
	}
	return $str;
}

function error_handle_with_dismiss($error = []){

  $str = '<div class="row" id="error" style="display:'.(!empty($error) ? '': 'none').'" >';
if(!empty($error)){
  foreach ($error as $key => $value) {
    $str .= '<div class="alert alert-danger alert-dismissible fade show col-sm-12" role="alert">'.$value.'
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button></div>
';
  }
}
  $str .= '</div>';
  echo $str;
}

function done_handle_with_dismiss($done = []){

  $str = '<div class="row" id="done" style="display:'.(!empty($done) ? '': 'none').'" >';
if(!empty($done)){
  foreach ($done as $key => $value) {
    $str .= '<div class="alert alert-success alert-dismissible fade show col-sm-12" role="alert">'.$value.'
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button></div>
';
  }
}
  $str .= '</div>';
  echo $str;
}

function aPOSTval($name, $default = ''){
	
	return get_safe_string(!empty($_POST) ? (!isset($_POST[$name]) ? '' : trim($_POST[$name])) : $default);

}

function redirect($url){
	global $globals;
	header('Location: '.$url);
	die();
}

function ssdatetimeformatter($datetime){
	return date('M j Y g:i A', strtotime($datetime));	
}

function ss_print($arr = []){
	echo '<pre>';print_r($arr); echo '</pre>';
}

function ss_dump(){
  $arg_list = func_get_args();

  echo '<pre>';
  for($i=0; $i < count($arg_list); $i++){
    var_dump($arg_list[$i]); 
  }
  echo '</pre>';
  // var_dump($arg_list);
}

function isadmin(){
	$role = !empty($_SESSION['user_details']['role']) ? $_SESSION['user_details']['role'] : 'user';

	if($role != 'admin'){
		return false;
	}
	return true;
}

function islogin(){
	$_user = !empty($_SESSION['user_details']) ? $_SESSION['user_details'] : [];

	if(!empty($_user)){
		return $_user;
	}

	return false;
}

function get_youtube_id_from_url($url){
    if (stristr($url,'youtu.be/'))
        {preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); return $final_ID[4]; }
    else 
        {@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); return $IDD[5]; }
}

function crate_button($url = '#', $size=3){
	echo '	
<div class="row justify-content-center" style="position:fixed;bottom:5px;right:5px;margin:0;padding:5px 3px;z-index: 10000;">
	<a href="'.$url.'"><i class="fas fa-plus-circle fa-'.$size.'x"></i></a>
</div>
';
}

function share_button($url = '#', $heading='', $size=2){
  $url = urlencode($url);
  $heading = urlencode($heading);

  $share = [];

  $share['fb'] = '<a class="fb" rel="nofollow noreferrer noopener" target="_blank" href="https://www.facebook.com/share.php?u='.$url.'" data-link="https://www.facebook.com/share.php?u='.$url.'"><i class="fab fa-facebook-f fa-'.$size.'x"></i></a>';

  $share['tw'] = '<a class="nc_tweet swp_share_link" rel="nofollow noreferrer noopener" target="_blank" href="https://twitter.com/intent/tweet?text='.$heading.';url='.$url.'" data-link="https://twitter.com/intent/tweet?text='.$heading.';url='.$url.'"><i class="fab fa-twitter fa-'.$size.'x"></i></a>';

  $share['wt'] = '<a href="whatsapp://send?text='.$url.'" data-action="share/whatsapp/share"><i class="fab fa-whatsapp fa-'.$size.'x"></i></a>';

  $str = '';
  foreach ($share as $key => $value) {
    $str .= $value;
  }
  return $str;
// 	return '
// <a href="https://www.instagram.com/?u='.$url.'"><i class="fab fa-instagram"></i></a>&nbsp;<a href="http://www.facebook.com/sharer.php/?u='.$url.'"><i class="fab fa-facebook-f"></i></a>&nbsp;<a href="https://twitter.com/share?url='.$url.'"><i class="fab fa-twitter"></i></a>
// 	';
}


function loader(){

	echo '
<style>
/* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 50px;
  height: 50px;
}

/* Transparent Overlay */
.loading:before {
  content: "";
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(255,255,255,0.5);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: "";
  display: block;
  font-size: 10px;
  width: 50px;
  height: 50px;
  margin-top: -0.5em;

  border: 15px solid rgba(33, 150, 243, 1.0);
  border-radius: 100%;
  border-bottom-color: transparent;
  -webkit-animation: spinner 1s linear 0s infinite;
  animation: spinner 1s linear 0s infinite;


}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<div class="loading" style="display:none">Loading&#8230</div>
';
}

function get_event_p_type(){
  return [
    'audience',
    'performer',
  ];
}

function get_event_cat_type(){
  return [
    'poet',
    'shayar',
    'singer',
    'stand up comedian',
    'rapper',
    'storyteller',
  ];
}

function getenvs(){

  global $globals;

  $data = file_get_contents(__DIR__.'/.env.json');
  return json_decode($data, true);

}

function env($key, $default = ''){
global $globals;
  $data = file_get_contents(__DIR__.'/.env.json');
  $data = json_decode($data, true);

  if(!empty($data[$key])){
    return $data[$key];
  }

  return $default;
}

function gettoken(){
  return !empty($_SESSION['token']) ? $_SESSION['token'] : '';
}

function makelog(){
  $arg_list = func_get_args();
  $str = file_get_contents(__DIR__.'/logs/runtime.log');
  foreach ($arg_list as $key => $value) {
      $str .= PHP_EOL;
      foreach ($value as $k => $v) {
        $str .=  '\''.$k.'='.$v.'\' ';
      }
  }
  // ss_print($str);exit;
  // echo "<pre>";var_dump($str);exit;

  file_put_contents(__DIR__.'/logs/runtime.log', $str);
}

function isJson($str) {
    $json = json_decode($str);
    return $json && $str != $json;
}

function template_ticket($email, $id){
  global $globals;

  $username = explode("@",$email);

  $str = '
<!DOCTYPE html>
<html>
<head>
  <title>Performer Ticket</title>
  <style type="text/css">
.ticket-container{
  border: 1px solid red;
  height: 600px;
  width: 337px;
  background-image: url(\'https://dev.standskills.in/asset/img/pticket.jpeg\');
  background-size: cover;
}

.img1{
  margin-left: 40%;
}

.date {
    font-size: 21px;
    position: absolute;
    top: 128px;
    left: 151px;
}

.performer {
    font-size: 25px;
    position: absolute;
    top: 401px;
    left: 135px;

.time {
 font-size: 25px;
    position: absolute;
    top: 471px;
    left: 162px;
}

.audience {
   font-size: 21px;
    position: absolute;
    top: 152px;
    left: 174px;
}





  </style>
</head>
<body>
  <div id="ticket" class="ticket-container">
    
      <div class="date">15 July 2021</div>
      <div class="performer">PARVEZ</div>
      <div class="time" style="font-size: 23px;
    position: absolute;
        top: 438px;
    left: 187px;">03:00</div>
      <div class="audience"  style="font-size: 23px;
    position: absolute;
        top: 569px;
    left: 216px;">01</div>
    
  </div>
</body>
</html>';

return $str;
}

function get_req_fields(){
  return ["id", "phone", "email", "buyer_name", "amount", "purpose", "expires_at", "status", "send_sms", "send_email", "sms_status", "email_status", "shorturl", "longurl", "redirect_url", "webhook", "allow_repeated_payments", "customer_id", "created_at", "modified_at"];

}

function event_plugin($data){
  global $globals;

  $str = '';

  if(empty($data['data'])){
    return $str;
  }
  // ss_print($data);
  foreach ($data['data'] as $key => $value) {
    $str .= '
<h2>'.$data['label'].'</h2>
<div class="row ssbg">
  <div class="col-sm-4">
    <a href="'.$globals['site_url'].'?act=event&register=1&event_id='.$value['event_id'].'">click here to register our event </a>
  </div>
  <div class="col-sm-4">
    <img class="img rounded mx-auto d-block img-thumbnail of event_image" data-toggle="modal" data-target="#imagemodal" src="'.$globals['events_path'].'/'.$value['event_image'].'" alt="'.$value['event_name'].'" >
    <div class="share-button text-right">
      '.share_button($globals['events_path'].'/'.$value['event_image'], $value['event_name'], 2).'
    </div>
  </div>
  <div class="col-sm-4">
    <div class="sticky-notes">
      <div class="sticky">
        <h2 class="eh">Event Name</h2>
            <p class="ed">'.$value['event_name'].'</p>
      </div>
      <div class="sticky">
        <h2 class="eh">Event Address</h2>
            <p class="ed">'.$value['event_add'].'</p>
      </div>
      <div class="sticky">
        <h2 class="eh">Event Details</h2>
            <p class="ed">'.$value['event_details'].'</p>
      </div>
      <div class="sticky">
        <h2 class="eh">Event DataTime</h2>
            <p class="ed">'.ssdatetimeformatter($value['event_date'].' '.$value['event_time']).'</p>
      </div>  
    </div>
  </div>
</div>

    ';    
  }

  return $str;  
}

function create_table_query($table_name, $table_structure = [], $ref = [], $timestamp = true){
  global $globals, $error;

  $query = 'CREATE TABLE IF NOT EXISTS '.$table_name;

  if(!empty($table_structure)){
    $query .=' (';
    $scount = count($table_structure);
    $si = 0;
    foreach ($table_structure as $key => $value){
      $query .= ' '.$key.' '.$value;
      if(++$si != $scount){
        $query .= ',';
      }
    }
     // $query .=')';
  }

  if(!empty($timestamp)){
    $query .= ', created TIMESTAMP  NULL default now(),
          updated TIMESTAMP NULL default now() on update now()';
  }

  if(!empty($ref)){
    $query .= ',';
    $rcount = count($ref);
    $ri = 0;
    foreach ($ref as $rk => $rv){
      $query .= ' FOREIGN KEY ('.$rk.') REFERENCES '.$rv['table'].'('.$rv['table_field'].')';
    }

    if(++$ri != $rcount){
      $query .= ',';
    } 
  }

  $query .= ')';
  return $query;
}