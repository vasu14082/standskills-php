<?php

function create_payment_request($data, $post_data = []){
	global $globals, $theme, $MYCONN, $error, $done;
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://'.env('PAY_MODE', 'www').'.instamojo.com/api/1.1/payment-requests/');
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_HTTPHEADER,
	            array('X-Api-Key:'.env('API_KEY'),
	                  'X-Auth-Token:'.env('AUTH_TOKEN')));

	$payload = Array(
	    'purpose' => $data['purpose'],
	    'amount' => $data['amount'],
	    'phone' => $data['phone'],
	    'buyer_name' => $data['buyer_name'],
	    'redirect_url' => $data['redirect_url'],
	    'send_email' => env('PAY_SEND_EMAIL', false),
	    'webhook' => $data['webhook'],
	    'send_sms' => env('PAY_SEND_SMS', false),
	    'email' => $data['email'],
	    'allow_repeated_payments' => $data['allow_repeated_payments'],
	    // 'expires_at' => env('PAY_LINK_EXP', 300)
	);

	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
	$response = curl_exec($ch);
	curl_close($ch); 
	
	$response = json_decode($response);

	if(empty($response->success)){
		$error[] = 'Something went wrong while making payment';
		return false;
	}

	$res = (array)$response->payment_request;

	$result = [];
	foreach (get_req_fields() as $value) {
		if(array_key_exists($value, $res)){
			$result[$value] = $res[$value];
		}
	}

	if(!empty(islogin())){
		$_user = islogin();
		$result['user_id'] = $_user['user_id'];
	}

	$ret = $MYCONN->create_payment($result);

	if(empty($ret)){
		$error[] = 'Something went wrong while making payment';
		return false;
	}

	// if(!empty($post_data)){
	$post_data['payment_request_id'] = $res['id'];

	$ret = $MYCONN->create_payment_details($post_data);

	// ss_dump($post_data, $ret, $error);exit;
	if(empty($ret)){
		$error[] = 'Something went wrong while making payment';
		return false;
	}
	// }

	// var_dump(json_decode($response));exit;
	header('Location: '.$response->payment_request->longurl);
}