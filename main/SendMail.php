<?php
header('Access-Control-Allow-Origin: *');
function sendmail($email){
    global $globals, $MYCONN, $error, $done;

    if($email!=""){
        require_once($globals['document_root']."/PHPMailer/src/SMTP.php");
        require_once($globals['document_root']."/PHPMailer/src/POP3.php");  
        require_once($globals['document_root']."/PHPMailer/src/PHPMailer.php"); 
        require_once($globals['document_root']."/PHPMailer/src/Exception.php");

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $error[] = 'Please enter correct email';
            return false;
        }

        if(strlen(optPOST('mobile_no')) != 10){
            $error[] = 'Please enter correct mobile number';
            return false;
        }

        if(strlen(optPOST('password')) < 8){
            $error[] = 'our password should be atleast 8 digit';
            return false;
        }

        if(empty(optPOST('user'))){
            $error[] = 'Please enter user name';
            return false;
        }

        if(optPOST('password') != optPOST('cnf_password')){
            $error[] = 'password and Confirm password doesn\'t match';
            return false;
        }

        $ret = $MYCONN->create_user(optPOST('user'), optPOST('email'), optPOST('mobile_no'), optPOST('password'), 'user');

        if(empty($ret)){
            return false;
        }
        
        // $ret = $MYCONN->get_user_by_id()
        $mail = new PHPMailer\PHPMailer\PHPMailer();

        $message = "Hello $email,<br><br>";
        $message .= '<b>'.time().'</b>';
    	$message .= '<br> <a href="'.$globals['site_url'].'?act=verify&user_id='.$ret.'"> '.$globals['site_url'].'?act=verify&user_id='.$ret.'</a>';
        $message .= "<br>Regards,<br><br>";
        $message .= "<a href='standskills.in'>standskill.in</a>";
        
        $mail->IsSMTP();    
        //$mail->SMTPDebug  = 2; 
        $mail->SMTPAuth   = true;                  
        $mail->SMTPSecure = "tls";                 
        $mail->Host       = "smtp-relay.sendinblue.com";     
        $mail->Port       = 587;   
        $mail->Username = "vasu14082@gmail.com";
        $mail->Password ="NdtWXZ0SgJ3bYrnx"; 
        $mail->setFrom('no-reply@standskills.in');
        $mail->addAddress($email);
        //$mail->addAddress("pratibha.digiture@gmail.com");
        // $mail->addAddress("rahul.digiture@gmail.com");

        // $mail->addBCC("amar.digiture@gmail.com");
        // $mail->addBCC("pratibha.digiture@gmail.com");
        $mail->Subject = "click link to register ".$email;
        $mail->MsgHTML($message);    
        $mail->IsHTML(true); // send as HTML 

        $mail->CharSet="utf-8"; // use utf-8 character encoding
        $mail->XMailer = ' '; 

        if(!$mail->send()) 
        {
            $MYCONN->delete_user_by_id($ret);
            $error[] = "Mailer Error: " . $mail->ErrorInfo;
            return false;
        } 
        else 
        {
            $done['msg'] = "Email Successfully Send";
            return true;
        }
    }else{
        $error[] = "Kindly fill the email address";
        return false;
    }

}

function ss_sendmail($email, $subject, $message, $from = 'no-reply@standskills.in'){
    global $globals, $MYCONN, $error, $done;

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $error[] = 'Please enter correct email';
        return false;
    }
    require_once($globals['document_root']."/PHPMailer/src/SMTP.php");
    require_once($globals['document_root']."/PHPMailer/src/POP3.php");  
    require_once($globals['document_root']."/PHPMailer/src/PHPMailer.php"); 
    require_once($globals['document_root']."/PHPMailer/src/Exception.php");

    
    
    // $ret = $MYCONN->get_user_by_id()
    $mail = new PHPMailer\PHPMailer\PHPMailer();

    $message = $message;
    // $message .= '<b>'.time().'</b>';
    // $message .= '<br> <a href="'.$globals['site_url'].'?act=verify&user_id='.$ret.'"> '.$globals['site_url'].'?act=verify&user_id='.$ret.'</a>';
    // $message .= "<br>Regards,<br><br>";
    // $message .= "<a href='standskills.in'>standskill.in</a>";
    
    $mail->IsSMTP();    
    //$mail->SMTPDebug  = 2; 
    $mail->SMTPAuth   = true;                  
    $mail->SMTPSecure = "tls";                 
    $mail->Host       = "smtp-relay.sendinblue.com";     
    $mail->Port       = 587;   
    $mail->Username = "vasu14082@gmail.com";
    $mail->Password ="NdtWXZ0SgJ3bYrnx"; 
    $mail->setFrom($from);
    $mail->addAddress($email);
    //$mail->addAddress("pratibha.digiture@gmail.com");
    // $mail->addAddress("rahul.digiture@gmail.com");

    // $mail->addBCC("amar.digiture@gmail.com");
    // $mail->addBCC("pratibha.digiture@gmail.com");
    $mail->Subject = $subject;
    $mail->MsgHTML($message);    
    $mail->IsHTML(true); // send as HTML 

    $mail->CharSet="utf-8"; // use utf-8 character encoding
    $mail->XMailer = ' '; 

    if(!$mail->send()) 
    {
        $error[] = "Mailer Error: " . $mail->ErrorInfo;
        return false;
    }

    $done['msg'] = "Email Successfully Send";
    return true;

}