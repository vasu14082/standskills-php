<?php

/**
 * 
 */
class MysqlConnect
{
	public $conn;
	function __construct()
	{
		global $globals;
		
		$this->conn = mysqli_connect(env('DB_HOST'), env('DB_USER'), env('DB_PASS'));
		if (!$this->conn) {
		    die('database not connected: ' . mysqli_connect_errno());
		}
	}

	function create_database(){
		global $globals;
		$query = 'CREATE DATABASE IF NOT EXISTS '.env('DB_NAME');
		mysqli_query($this->conn, $query);
		return mysqli_select_db($this->conn, env('DB_NAME'));
	}

	function create_user_table(){

		$query = 'CREATE TABLE IF NOT EXISTS users (user_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					user_name VARCHAR(50) NOT NULL,
					email VARCHAR(30) UNIQUE KEY NOT NULL,
					mobile_no VARCHAR(15) UNIQUE KEY DEFAULT 0,
					password VARCHAR(100) NOT NULL,
					role VARCHAR(20) DEFAULT "user",
					isverify int(2) NOT NULL DEFAULT 0 )';
		if(!mysqli_query($this->conn, $query)){
			return mysqli_error($this->conn);
		}
		return true;
	}

	function get_tables_list(){
		$query = 'SHOW TABLES';
		$result = mysqli_query($this->conn, $query);

		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_row($result)) {
			array_push($ret, $row[0]);
		}

		return $ret;
	}

	function create_user($user, $email, $mobile_no, $pass, $role = 'user', $isverify = 0){
		if(!in_array('users', $this->get_tables_list())){
			$this->create_user_table();
		}

		$query = 'INSERT INTO users (user_name, email, mobile_no, password, role, isverify) VALUES ("'.$user.'", "'.$email.'", "'.$mobile_no.'", "'.password_hash($pass, PASSWORD_DEFAULT).'", "'.$role.'", '.$isverify.')';

		if(!mysqli_query($this->conn, $query)){
			return mysqli_error($this->conn);
		}

		return mysqli_insert_id($this->conn);
		// return true;
	}

	function get_user_name_by_id($id){

		$query = 'SELECT user_name FROM users WHERE user_id='.$id;

		$result = mysqli_query($this->conn, $query);

		$ret = '';
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_row($result)) {
			return $row[0];
		}

		return $id;
	}

	function get_user_by_id($id){

		$query = 'SELECT * FROM users WHERE user_id='.$id;

		$result = mysqli_query($this->conn, $query);

		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)) {
			return $row;
		}

		return $ret;
	}

	function get_user_by_email($email){

		$query = 'SELECT * FROM users WHERE email="'.$email.'"';

		$result = mysqli_query($this->conn, $query);

		$ret = [];
		if(empty($result)){
			return $ret;
		}
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)) {
			return $row;
		}

		return $ret;
	}

	function delete_user_by_id($id){

		$query = 'DELETE FROM users WHERE user_id='.$id;
		$result = mysqli_query($this->conn, $query);

		return $result;
	}

	function verify_user($id, $isverify=1){
	global $globals, $error;
		$query = 'UPDATE users SET isverify='.$isverify.' WHERE user_id='.$id;
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function update_password_user($email, $password){
	global $globals, $error;
		$query = 'UPDATE users SET password="'.password_hash($password, PASSWORD_DEFAULT).'" WHERE email="'.$email.'"';
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function do_login($email, $pass){
		global $globals, $error;

		$query = 'SELECT * FROM users WHERE email="'.$email.'"';
		$result = mysqli_query($this->conn, $query);

		if(empty($result)){
			$error[] = 'User doesn\'t exists';
			return false;
		}

		$ret = mysqli_fetch_assoc($result);
		if(!password_verify($pass, $ret['password'])){
			$error[] = 'Please enter correct password';
			return false;
		}

		if(empty($ret['isverify'])){
			$error[] = 'You account is not verified';
			return false;
		}
		
		$_SESSION['user_details'] = $ret;
		$_SESSION['token'] = bin2hex(random_bytes(32));

		return true;
	}

	function do_logout(){
		session_unset();
		session_destroy();
		mysqli_close($this->conn);
	}

	function get_user_details(){

		if(!empty($_SESSION['user_details'])){
			return $_SESSION['user_details'];
		}

		return [];
	}

	function close_conn(){
		mysqli_close($this->conn);
	}

	function create_post_table(){
	global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS posts (
					post_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					shayri VARCHAR(300) NOT NULL DEFAULT "",
					shayri_image VARCHAR(50) NOT NULL DEFAULT "",
					user_id INT(10) UNSIGNED,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (user_id) REFERENCES  	(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;	
	}

	function create_post($user_id, $shayri = '', $shayri_image=''){
	global $globals, $error;

		if(!in_array('posts', $this->get_tables_list())){
			$this->create_post_table();
		}

		$query = 'INSERT INTO posts(shayri, shayri_image, user_id) VALUES ("'.$shayri.'", "'.$shayri_image.'", '.$user_id.')';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function create_event_table(){
	global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS events (
					event_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					event_name VARCHAR(200) NOT NULL DEFAULT "",
					event_add VARCHAR(300) NOT NULL DEFAULT "",
					event_details VARCHAR(500) NOT NULL DEFAULT "",
					event_image VARCHAR(100) NOT NULL DEFAULT "",
					event_date VARCHAR(50) NOT NULL DEFAULT "",
					event_time VARCHAR(50) NOT NULL DEFAULT "",
					event_price JSON,
					user_id INT(10) UNSIGNED,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_event($user_id, $event_name, $event_add, $event_details, $event_date, $event_time, $event_image, $event_price = "{}"){
	global $globals, $error;

		if(!in_array('events', $this->get_tables_list())){
			$this->create_event_table();
		}

		$query = 'INSERT INTO events( user_id, event_name, event_add, event_details, event_date, event_time, event_image, event_price ) VALUES ('.$user_id.', "'.$event_name.'", "'.$event_add.'", "'.$event_details.'", "'.$event_date.'", "'.$event_time.'", "'.$event_image.'", \''.$event_price.'\')';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function get_events($page = 0){
		global $globals, $error;

		$query = 'SELECT events.* , users.* FROM events LEFT JOIN users ON events.user_id=users.user_id ORDER BY events.created DESC LIMIT '.$page.', '.$globals['event_per_page'];

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while($row = mysqli_fetch_assoc($result)){
			$_user = islogin();
			if(!empty($_user)){
				$ret1 = $this->get_notification_from_email($row['event_id'], $_user['email']);
				$row['is_notify'] = !empty($ret1['is_notify']) ? 1 : 0;	
			}
			$ret[$row['event_id']] = $row;
		}

		return $ret;
	}

	function get_event_by_id($event_id){
		global $globals, $error;

		$query = 'SELECT * FROM events WHERE event_id='.$event_id;

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return [];
		}
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)){
			return $row;
		}

		return $ret;
	}

	function create_event_notification_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS events_notification (
					event_notify_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					event_id INT(10) UNSIGNED,
					email VARCHAR(30) NOT NULL,
					is_notify BIT(1) DEFAULT 0,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (event_id) REFERENCES events(event_id)
				)';
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_event_notification($event_id, $email, $is_notify=0){
	global $globals, $error;

		if(!in_array('events_notification', $this->get_tables_list())){
			$this->create_event_notification_table();
		}

		$query = 'INSERT INTO events_notification(event_id, email, is_notify) VALUES ('.$event_id.', "'.$email.'", '.$is_notify.')';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function get_event_notfication($event_id){
		global $globals, $error;

		$query = 'SELECT * FROM events_notification WHERE event_id='.$event_id.' ORDER BY created DESC';

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)){
			$ret[] = $row;
		}

		return $ret;
	}

	function update_event_notification($event_notify_id, $is_notify){
		global $globals, $error;

		$query = 'UPDATE events_notification SET is_notify='.(!empty($is_notify) ? 1 : 0 ).' WHERE event_notify_id='.$event_notify_id;

		// var_dump($query);

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function get_notification_from_email($event_id, $email){
		global $globals, $error;

		$query = 'SELECT * FROM events_notification WHERE event_id='.$event_id.' AND email="'.$email.'"';
		// var_dump($query);exit;
		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $result;
		}

		while($row = mysqli_fetch_assoc($result)){
			$ret = $row; 
			// array_push($ret, $row);
		}

		return $ret;
	}

	function create_blog_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS blogs (
					blog_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					blog_title VARCHAR(300) NOT NULl,
					blog_detail VARCHAR(10000) NOT NULL DEFAULT "",
					blog_image VARCHAR(100) NOT NULL DEFAULT "",
					user_id INT(10) UNSIGNED,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_blog($user_id, $blog_title, $blog_detail, $blog_image=''){
	global $globals, $error;

		if(!in_array('blogs', $this->get_tables_list())){
			$this->create_blog_table();
		}

		$query = 'INSERT INTO blogs(blog_title, blog_detail, blog_image, user_id) VALUES ("'.$blog_title.'", "'.$blog_detail.'", "'.$blog_image.'", '.$user_id.')';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function get_blogs($page = 0){
		global $globals, $error;

		$query = 'SELECT blogs.* , users.user_name FROM blogs LEFT JOIN users ON blogs.user_id=users.user_id ORDER BY blogs.created DESC LIMIT '.$page.', '.$globals['blog_per_page'];

		$result = mysqli_query($this->conn, $query);

		$response = [];

		if(!empty($result)){
			while ($row = mysqli_fetch_assoc($result)){
				$_user = islogin();

				if(!empty($_user)){
					$row['likes_blog'] = $this->get_blog_like($_user['user_id'], $row['blog_id']);
				}

				$row['likes_count'] = $this->get_blog_likes_count($row['blog_id']);
				$row['comment_count'] = $this->get_blog_comments_count($row['blog_id']);
				$response['data'][$row['blog_id']] = $row;
			}
		}

		$response['total'] =  $this->get_blog_count();
		$response['page'] = $page;
		$response['fetched'] = count($response['data']);

		return $response;
	}

	function get_blog_by_id($blog_id){
		global $globals, $error, $_user;

		$query = 'SELECT * FROM blogs WHERE blog_id='.$blog_id;

		$result = mysqli_query($this->conn, $query);

		$response = [];
		if(empty($result)){
			return $response;
		}

		while($row = mysqli_fetch_assoc($result)){
			if(!empty($_user['user_id'])){
				$row['likes_blog'] = $this->get_blog_like($_user['user_id'], $row['blog_id']);
			}
			$row['user_name'] = $this->get_user_name_by_id($row['user_id']);
			$row['likes_count'] = $this->get_blog_likes_count($row['blog_id']);
			$row['comment_count'] = $this->get_blog_comments_count($row['blog_id']); 
			$response = $row;
		}

		return $response;
	}

	function get_blog_count(){
		global $globals, $error;

		$query = 'SELECT count(blog_id) AS count FROM blogs';
		// var_dump($query);exit;
		$ret = 0;
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $ret;
		}

		while($row = mysqli_fetch_assoc($result)){
			return $row['count']; 
			// array_push($ret, $row);
		}

		return $ret;
	}

	function craete_blog_like_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS blog_likes (
					blog_like_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					blog_id INT(10) UNSIGNED,
					user_id INT(10) UNSIGNED,
					is_like BIT(1) DEFAULT 0,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (blog_id) REFERENCES blogs(blog_id)  ON DELETE CASCADE  ON UPDATE CASCADE,
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_blog_like($user_id, $blog_id, $is_like = 0){
	global $globals, $error;

		if(!in_array('blog_likes', $this->get_tables_list())){
			$this->craete_blog_like_table();
		}

		$query = 'INSERT INTO blog_likes(blog_id, user_id, is_like) VALUES ('.$blog_id.', '.$user_id.', '.$is_like.')';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function get_blog_like($user_id, $blog_id){
		global $globals, $error;

		$query = 'SELECT * FROM blog_likes WHERE blog_id='.$blog_id.' AND user_id='.$user_id;

		$result = mysqli_query($this->conn, $query);

		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)){
			return $row;
		}

		return $ret;
	}

	function update_blog_like($blog_like_id, $is_like = 0){
		global $globals, $error;

		$query = 'UPDATE blog_likes SET is_like='.(!empty($is_like) ? 1 : 0 ).' WHERE blog_like_id='.$blog_like_id;

		// var_dump($query);

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function get_blog_likes_by_user($user_id){
		global $globals, $error;

		$query = 'SELECT * FROM blog_likes WHERE user_id='.$user_id;
		// var_dump($query);exit;
		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $ret;
		}

		while($row = mysqli_fetch_assoc($result)){
			$ret[$row['blog_id']] = $row; 
			// array_push($ret, $row);
		}

		return $ret;

	}

	function get_blog_likes_count($blog_id, $is_like=1){
		global $globals, $error;

		$query = 'SELECT count(blog_id) AS count FROM blog_likes WHERE blog_id='.$blog_id.' AND is_like='.$is_like;
		// var_dump($query);exit;
		$ret = 0;
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $ret;
		}

		while($row = mysqli_fetch_assoc($result)){
			return $row['count']; 
			// array_push($ret, $row);
		}

		return $ret;
	}

	function craete_blog_comment_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS blog_comments (
					blog_comment_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					blog_id INT(10) UNSIGNED,
					user_id INT(10) UNSIGNED,
					comment VARCHAR(500) NOT NULL DEFAULT "",
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (blog_id) REFERENCES blogs(blog_id)  ON DELETE CASCADE  ON UPDATE CASCADE,
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_blog_comment($user_id, $blog_id, $comment = '', $get_comment_too = false){
	global $globals, $error;

		if(!in_array('blog_comments', $this->get_tables_list())){
			$this->craete_blog_comment_table();
		}

		$query = 'INSERT INTO blog_comments(blog_id, user_id, comment) VALUES ('.$blog_id.', '.$user_id.', "'.$comment.'")';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		$ret = [];
		if(!empty($get_comment_too)){
			$blog_comment_id = mysqli_insert_id($this->conn);
			$selectquery = 'SELECT * FROM blog_comments WHERE blog_comment_id='.$blog_comment_id;

			$res = mysqli_query($this->conn, $selectquery);

			if(empty($res)){
				return $ret;
			}

			while ($r = mysqli_fetch_assoc($res)){
				$r['user_name'] = $this->get_user_name_by_id($r['user_id']);
				return $r;
			}
		}		

		return true;

	}

	function get_blog_comment($user_id, $blog_id){
		global $globals, $error;

		$query = 'SELECT * FROM blog_comments WHERE blog_id='.$blog_id.' AND user_id='.$user_id;

		$result = mysqli_query($this->conn, $query);

		$ret = [];
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		while ($row = mysqli_fetch_assoc($result)){
			$ret[$row['blog_id']] = $row;
		}

		return $ret;
	}

	function get_blog_comment_by_blog_id($blog_id, $page=0){
		global $globals, $error;

		$query = 'SELECT * FROM blog_comments WHERE blog_id='.$blog_id.' ORDER BY created DESC LIMIT '.$page.', '.$globals['limit'];

		$result = mysqli_query($this->conn, $query);

		$response = [];

		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);

		if(!empty($result)){
			$response['data'] = [];
			while ($row = mysqli_fetch_assoc($result)){
				$row['user_name'] = $this->get_user_name_by_id($row['user_id']);
				$response['data'][] = $row;
			}
		}

		$response['total'] = $this->get_blog_comments_count($blog_id);
		$response['fetched'] = count($response['data']);
		$response['page'] = $page;

		return $response;
	}

	function get_blog_comments_count($blog_id){
		global $globals, $error;

		$query = 'SELECT count(blog_comment_id) AS count FROM blog_comments WHERE blog_id='.$blog_id;
		// var_dump($query);exit;
		$ret = 0;
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $ret;
		}

		while($row = mysqli_fetch_assoc($result)){
			return $row['count']; 
			// array_push($ret, $row);
		}

		return $ret;
	}

	function create_videos_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS videos (
					video_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					user_id INT(10) UNSIGNED,
					video_link VARCHAR(1000) NOT NULL DEFAULT "",
					video_title VARCHAR(500) NOT NULL DEFAULT "",
					video_author VARCHAR(100) NOT NULL DEFAULT "",
					video_type VARCHAR(50) NOT NULL DEFAULT "youtube",
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_videos($user_id, $video_link, $video_title = '', $video_author = '', $video_type='youtube'){
	global $globals, $error;

		if(!in_array('videos', $this->get_tables_list())){
			$this->create_videos_table();
		}

		$query = 'INSERT INTO videos (user_id, video_link, video_title, video_author, video_type) VALUES ('.$user_id.', "'.$video_link.'", "'.$video_title.'", "'.$video_author.'", "'.$video_type.'")';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}	

		return true;

	}

	function get_videos($page = 0){
		global $globals, $error;

		$query = 'SELECT * FROM videos ORDER BY created DESC LIMIT '.$page.', '.$globals['video_per_page'];

		$result = mysqli_query($this->conn, $query);

		$response = [];

		if(!empty($result)){
			while ($row = mysqli_fetch_assoc($result)){
				$response['data'][$row['video_id']] = $row;
			}
		}

		$query = 'SELECT count(*) as total FROM videos';

		$result = mysqli_query($this->conn, $query);

		$total = 0;

		if(!empty($result)){
			$row = mysqli_fetch_assoc($result);
			$total = $row['total'];
		}

		$response['total'] = $total;
		$response['page'] = $page;
		$response['fetched'] = count($response['data']);

		return $response;
	}

	function get_yt_video_by_link($video_link){
		global $globals, $error;

		$query = 'SELECT * FROM videos WHERE video_link="'.$video_link.'"';

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}

		while ($row = mysqli_fetch_assoc($result)){
			$ret = $row;
		}

		return $ret;
	}

	function create_gallery_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS gallery (
					gallery_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					user_id INT(10) UNSIGNED,
					gallery_path VARCHAR(500) NOT NULL DEFAULT "",
					insta_id VARCHAR(500) NOT NULL DEFAULT "",
					ishome BIT(1) DEFAULT 0,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now(),
					fOREIGN KEY (user_id) REFERENCES users(user_id)  ON DELETE CASCADE  ON UPDATE CASCADE
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_gallery($user_id, $gallery_path, $insta_id = ''){
	global $globals, $error;

		if(!in_array('gallery', $this->get_tables_list())){
			$this->create_gallery_table();
		}

		$query = 'INSERT INTO gallery (user_id, gallery_path, insta_id) VALUES ('.$user_id.', "'.$gallery_path.'", "'.$insta_id.'")';

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}	

		return true;

	}

	function get_gallery($page = 0){
		global $globals, $error;

		$query = 'SELECT * FROM gallery ORDER BY created DESC LIMIT '.$page.', '.$globals['gallery_per_page'];

		$result = mysqli_query($this->conn, $query);

		$response = [];

		if(!empty($result)){
			$response['data'] = [];
			while ($row = mysqli_fetch_assoc($result)){
				$response['data'][] = $row;
			}
		}

		$response['fetched'] = count($response['data']);
		$response['page'] = $page;
		$response['total'] = $this->get_gallery_count();
		return $response;
	}

	function get_gallery_count(){
		global $globals, $error;

		$query = 'SELECT count(gallery_id) AS count FROM gallery';
		// var_dump($query);exit;
		$ret = 0;
		// $ret = mysqli_fetch_all($result, MYSQLI_ASSOC);
		$result = mysqli_query($this->conn, $query);
		
		if(empty($result)){
			return $ret;
		}

		while($row = mysqli_fetch_assoc($result)){
			return $row['count']; 
			// array_push($ret, $row);
		}

		return $ret;
	}

	function get_gallery_by_id($gallery_id){
		global $globals, $error;

		$query = 'SELECT * FROM gallery WHERE gallery_id='.$gallery_id;

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}

		while ($row = mysqli_fetch_assoc($result)){
			$ret = $row;
		}

		return $ret;
	}

	function set_gallery_as_home($gallery_id, $ishome = 0){
		global $globals, $error;

		$query = 'UPDATE gallery SET ishome='.(!empty($ishome) ? 1 : 0 ).' WHERE gallery_id='.$gallery_id;

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function get_home_gallery($ishome=1){
		global $globals, $error;

		$query = 'SELECT * FROM gallery WHERE ishome='.$ishome;

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}

		while ($row = mysqli_fetch_assoc($result)){
			$ret[] = $row;
		}

		return $ret;

	}

	function get_home_data(){
		global $globals, $error;

		$response = [];

		// get Upcomming event;
		$query = 'SELECT events.* , users.* FROM events LEFT JOIN users ON events.user_id=users.user_id ORDER BY events.created DESC LIMIT 0, 1';
		$result = mysqli_query($this->conn, $query);

		if(!empty($result)){
			$response['event'] = [];
			$response['event']['label'] = 'Our Upcomming Event';
			while ($row = mysqli_fetch_assoc($result)){
				$response['event']['data'][] = $row;
			}
		}

		// Lates blog
		$query = 'SELECT blogs.* , users.user_name FROM blogs LEFT JOIN users ON blogs.user_id=users.user_id ORDER BY blogs.created DESC LIMIT 0, 4';
		$result = mysqli_query($this->conn, $query);

		if(!empty($result)){
			$response['blogs'] = [];
			$response['blogs']['label'] = 'Our Most Recents blogs';
			while ($row = mysqli_fetch_assoc($result)){
				$response['blogs']['data'][] = $row;
			}
		}

		return $response;
	}

	function delete_gallery_by_id($gallery_id){
		global $globals, $error;

		$query = 'DELETE FROM gallery WHERE gallery_id='.$gallery_id;

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;

	}

	function create_payment_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS payment (
					ss_pay_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					id VARCHAR(1000) DEFAULT "",
					payment_id VARCHAR(1000) DEFAULT "",
					customer_id VARCHAR(1000) DEFAULT "",
					user_id INT(10) UNSIGNED DEFAULT 0,
					email VARCHAR(80) DEFAULT "",
					phone VARCHAR(15) DEFAULT 0,
					buyer_name VARCHAR(200) DEFAULT "",
					amount VARCHAR(200) DEFAULT 0,
					purpose VARCHAR(1000) DEFAULT "",
					status VARCHAR(50) DEFAULT "Pending",
					send_sms VARCHAR(10) DEFAULT "false",
					send_email VARCHAR(10) DEFAULT "false",
					sms_status VARCHAR(50) DEFAULT "Pending",
					email_status VARCHAR(10) DEFAULT "Pending",
					shorturl VARCHAR(1000) DEFAULT "",
					longurl VARCHAR(1000) DEFAULT "",
					redirect_url VARCHAR(1000) DEFAULT "",
					webhook VARCHAR(1000) DEFAULT "",
					created_at VARCHAR(100) DEFAULT "",
					modified_at VARCHAR(100) DEFAULT "",
					expires_at VARCHAR(100) DEFAULT "",
					allow_repeated_payments VARCHAR(10) DEFAULT "false",
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now()
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_payment($data){
	global $globals, $error;

		if(!in_array('payment', $this->get_tables_list())){
			$this->create_payment_table();
		}

		$query = 'INSERT INTO payment('.implode(', ', array_keys($data)).') VALUES ( ';

		$numItems = count($data);
		$i = 0;
		foreach ($data as $key => $value) {
			
			if(is_int($value)){
				$query .= $value;
			}

			if(is_string($value)){
				$query .= '"'.$value.'"';
			}

			if(is_bool($value)){
				if(!empty($value)){
					$query .= '"true"';
				}else{
					$query .= '"false"';
				}
			}

			if(is_null($value)){
				$query .= '""';
			}

			if(++$i !== $numItems) {
			    $query .= ', ';
			 }
		}

		$query .= ')';

		// return $query;

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function update_payment($data, $id){
	global $globals, $error;

		if(!in_array('payment', $this->get_tables_list())){
			$error[] = "something went wrong";
			return false;
		}

		$query = 'UPDATE payment SET ';

		$numItems = count($data);
		$i = 0;
		foreach ($data as $key => $value) {

			if(is_int($value)){
				$query .= $key.'='.$value;
			}

			if(is_string($value)){
				$query .= $key.'="'.$value.'"';
			}

			if(is_bool($value)){
				if(!empty($value)){
					$query .= $key.'="true"';
				}else{
					$query .= $key.'="false"';
				}
			}

			if(is_null($value)){
				$query .= $key.'=""';
			}

			if(++$i !== $numItems) {
			    $query .= ', ';
			 }
		}

		$query .= ' WHERE id="'.$id.'"';

		// return $query;

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function create_payment_details_table(){
		global $globals, $error;
		$query = 'CREATE TABLE IF NOT EXISTS payment_details (
					id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					payment_request_id VARCHAR(100),
					payment_id VARCHAR(100) DEFAULT "",
					payment_type_id VARCHAR(100) DEFAULT "",
					payment_type VARCHAR(100) DEFAULT "",
					payment_status VARCHAR(100) DEFAULT "",
					payment_amount VARCHAR(200) DEFAULT 0,
					payment_fields JSON,
					payment_bill_fields JSON,
					created TIMESTAMP  NULL default now(),
					updated TIMESTAMP NULL default now() on update now()
				)';
		
		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}
		return true;
	}

	function create_payment_details($data){
	global $globals, $error;

		if(!in_array('payment_details', $this->get_tables_list())){
			$this->create_payment_details_table();
		}

		$query = 'INSERT INTO payment_details('.implode(', ', array_keys($data)).') VALUES ( ';

		$numItems = count($data);
		$i = 0;
		foreach ($data as $key => $value) {
			++$i;
			if(is_int($value)){
				$query .= $value;
			}

			if(isJson($value)){
				$query .= '\''.$value.'\'';

				if($i !== $numItems) {
				    $query .= ', ';
				 }
				continue;
			}

			if(is_string($value)){
				$query .= '"'.$value.'"';
			}

			if(is_bool($value)){
				if(!empty($value)){
					$query .= '"true"';
				}else{
					$query .= '"false"';
				}
			}

			if(is_null($value)){
				$query .= '""';
			}

			if($i !== $numItems) {
			    $query .= ', ';
			 }
		}

		$query .= ')';

		// return $query;

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}

	function get_payment_details($user_id){
		global $globals, $error;

		$query = 'SELECT payment_details.* , payment.user_id FROM payment_details LEFT JOIN payment ON payment_details.payment_id=payment.payment_id WHERE payment.user_id='.$user_id.' ORDER BY payment_details.created DESC';

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}

		while ($row = mysqli_fetch_assoc($result)){
			$ret[] = $row;
		}

		return $ret;
	}

	function get_payment_detail($payment_details_id){
		global $globals, $error;

		$query = 'SELECT *  FROM payment_details WHERE payment_request_id="'.$payment_details_id.'"';

		$result = mysqli_query($this->conn, $query);

		$ret = [];

		if(empty($result)){
			return $ret;
		}

		while ($row = mysqli_fetch_assoc($result)){
			$ret = $row;
		}

		return $ret;
	}

	function update_payment_details($data, $id){
	global $globals, $error;

		if(!in_array('payment_details', $this->get_tables_list())){
			$error[] = "something went wrong";
			return false;
		}

		$query = 'UPDATE payment_details SET ';

		$numItems = count($data);
		$i = 0;
		foreach ($data as $key => $value) {

			++$i;

			if(is_int($value)){
				$query .= $key.'='.$value;
			}

			if(is_string($value)){
				$query .= $key.'="'.$value.'"';
			}

			if(is_bool($value)){
				if(!empty($value)){
					$query .= $key.'="true"';
				}else{
					$query .= $key.'="false"';
				}
			}

			if(isJson($value)){
				$query .= '\''.$value.'\'';
				continue;
			}
			
			if(is_null($value)){
				$query .= $key.'=""';
			}

			if($i !== $numItems) {
			    $query .= ', ';
			 }
		}

		$query .= ' WHERE payment_request_id="'.$id.'"';

		// return $query; 

		if(!mysqli_query($this->conn, $query)){
			$error[] = mysqli_error($this->conn);
			return false;
		}

		return true;
	}
}