<?php

function ssindex_theme(){

global $globals, $theme, $error, $done, $gallery, $homedata;
	// var_dump($theme);echo "exit";exit;
	ss_header(env('APP_NAME', 'standskills').'- HOME');
	ss_navigation();

  echo "
<style>
.imgbox {
    display: grid;
    height: 100%;
}
.center-fit {
    max-width: 100%;
    max-height: 70vh;
    margin: auto;
} 
</style>

";

$li = '';
$im = '';
foreach ($gallery as $key => $value){
	$li .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$key.'" '.($key == 0 ? 'class="active"' : '').'></li>';

  	 $im .='
    <div class="carousel-item '.($key == 0 ? 'active' : '').'">
      <img class="w-100 center-fit" src="'.$globals['gallery_path'].'/'.$value['gallery_path'].'" alt="First slide">
    </div>';
}

echo '
<div class="content row">

  <div id="carouselExampleIndicators" class="carousel slide col-sm-12" data-ride="carousel" data-interval="'.(env('HOME_IMG_SLIDE_TIME', 5)*1000).'">
    <ol class="carousel-indicators">
      '.$li.'
    </ol>
    <div class="carousel-inner" role="listbox">
      '.$im.'
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<script>
</script>';

ss_print($homedata);
// Event;
echo event_plugin($homedata['event']);

	// crate_button($globals['site_url'].'?act=addpost');
	ss_footer();

}