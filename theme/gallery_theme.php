<?php

function gallery_theme(){

global $globals, $theme,$error, $done, $addtogallery, $gallery;
	ss_header(env('APP_NAME', 'StandSkills').' - Gallery');
	ss_navigation('gallery');

echo '
<style>

// .scale {
//    width:100%;
//    height:400px;
//    object-fit:cover;
//    object-position:30% 30%;
//   }

.md-dialog {
      max-width: 800px;
      margin: 30px auto;
  }



.md-body {
  position:relative;
  padding:0px;
  min-height:400px;
  background:#ccc;
}




.cls {
  position:absolute;
  right:-30px;
  top:0;
  z-index:999;
  font-size:2rem;
  font-weight: normal;
  color:#fff;
  opacity:1;
}

#image {
  min-height:200px;
}

.flex_container {
	// display: flex;
	// flex-direction: row;
	// flex-wrap: wrap;
	// justify-content: center;

	/* It is split into 3 columns */
    column-count: 3	;

    /* The space between columns */
    // column-gap: 1rem;

    /* Misc */
    width: 100%;
}

.flex_item{
	 /* Prevent a column from breaking into multiple columns */
    break-inside: avoid;

    /* Misc */
    margin-bottom: 1rem;
}
</style>
';

if(isadmin()){
	crate_button($globals['site_url'].'?act=gallery&addtogallery=1');
}

loader();

error_handle_with_dismiss($error);

done_handle_with_dismiss($done);

if(!empty($addtogallery)){

echo '
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h2>Craete Gallery</h2>
		<form method="post" id="galleryform" enctype="multipart/form-data">
			<div class="form-group">
		        <label for="gallery_instaid">User Instagram Id</label>
		        <input type="text" id="gallery_instaid" class="form-control" name="gallery_instaid" '.aPOSTval('gallery_instaid', '').' />
		    </div>
		    <div class="form-group">
		        <label for="gallery_image"> Add Blog/Script Image</label>
		        <input type="file" id="gallery_image" name="gallery_image" accept="image/*" onchange="loadFile(event)">
		    </div>
		    <div class="form-group">
		        <label for="ishome"> Make this image visible to home </label>
		        <input type="checkbox" name="ishome" id="ishome">
		    </div>
		    <div class="form-group">
		    	<image id="preview">
		    </div>
		    <div class="form-group">
		        <input type="submit" name="create_gallary" class="btn btn-primary" value="Upload">
		        <a class="btn btn-danger" href="'.$globals['site_url'].'?act=gallery'.'">
		            Cancel
		        </a>
		    </div>
		</form>
	</div>
</div>
<script>
 var loadFile = function(event){
    var output = document.getElementById("preview");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };

  $("#galleryform").submit(function(e){
  	$("#error").html("");
  	let gallery_image = $("#gallery_image")[0].files;
  	let error_list = [];
  	$("#error").html("");
  	if(gallery_image.length < 1){
  		error_list.push("Please add gallery image");
  	}
  	var str = error_hand_with_dismiss(error_list);
   $("#error").html(str).show();

	if(error_list.length > 0){
		return false;
	}
  });
</script>';
} else {
echo '<br><div class="flex_container">';

foreach ($gallery['data'] as $key => $value) {
	if(!is_array($value)){
		continue;
	}
	echo '
		<div class="gallery flex_item" id="div_gallery_'.$value['gallery_id'].'">
			<img class="img-fluid scale" data-toggle="modal" data-target="#imagemodal" style="border:2px solid black" src="'.$globals['gallery_path'].'/'.$value['gallery_path'].'" alt="'.(!empty($value['insta_id']) ? $value['insta_id'] : $globals['instagram']).'">';
			
			if(!empty(isadmin())){
			echo '
			<div class="btn-group dropup">
			  <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
			    <span id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></span>
			  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" onclick="setAsHome(this)" data-gallery_id="'.$value['gallery_id'].'" data-ishome="'.$value['ishome'].'">'.(!empty($value['ishome']) ? 'Unset' : 'Set').' Image On home</a>
			    <a class="dropdown-item" data-gallery_id="'.$value['gallery_id'].'" data-gallery_name="'.$value['gallery_path'].'" onclick="delete_gallery(this)">Delete image</a>
			    <a class="dropdown-item" data-insta_id="'.(!empty($value['insta_id']) ? $value['insta_id'] : $globals['instagram']).'" data-gallery_id="'.$value['gallery_id'].'" data-toggle="modal" data-target="#exampleModal">change instagram id</a>
			  </div>
			</div>';
			}

			echo '
		</div>
	';
}

echo '</div>';

echo '<!-- Button trigger modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change instagram ID</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>';
      if(empty(islogin())){
      	echo '
      		<a class="btn btn-secondary" href="'.$globals['site_url'].'?act=login">Login To Comment</a>
      	';
      }else{
      echo 
	      '<form method="post" id="insta_form">
	      	 <div class="modal-body">
	        
		        	<input type="hidden" name="gallery_id" class="form-control" id="gallery_id">
		          <div class="form-group">
		            <label for="message-text" class="col-form-label">Instagram Id:</label>
		            <input type="text" id="insta_id" name="insta_id" class="form-control">
		          </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary" name="blog_comment">Comment</button>
		      </div>
	      </form>';
  		}
      echo
    '</div>
  </div>
</div>';

echo '
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog md-dialog" role="document">
    <div class="modal-content">

      
      <div class="modal-body md-body">

       <button type="button" class="close cls" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        

                <img src="//media.tenor.com/images/556e9ff845b7dd0c62dcdbbb00babb4b/tenor.gif" alt="" id="image" class="img-fluid">

        
        
      </div>

    </div>
  </div>
</div>
';

echo '
<script>

function setAsHome(ele){
	var gallery_id = $(ele).data("gallery_id");
	var ishome = parseInt($(ele).data("ishome")) == 0 ? 1 : 0;
	$(".loading").show();
	$.ajax({
		type: "POST",
		url: "'.$globals['site_url'].'?act=gallery&api=json&set_on_home=1",
		data: {"gallery_id": gallery_id, "ishome": ishome},
		dataType: "json",
		success: function(data){
			$(".loading").hide();
			// console.log("galler", $(ele).data("gallery_id"));
			// console.log(data);return false;
			if (data.error !== null || data.error !== undefined){
				// alert(data.done.msg);

				let str = data._gallery.ishome == "1" ? "Unset" : "Set" ;
				$(ele).data("ishome", data._gallery.ishome);
				$(ele).html(str+" Image On home");
			}else{
				alert(data.error.join("\n"));
				// alert("something went wrong");
			}
		},
		Error: function(error){
			alert("something went wrong");
		}
	});
}

function delete_gallery(ele){
	var gallery_id = $(ele).data("gallery_id");
	var gallery_name = $(ele).data("gallery_name");
	// console.log(gallery_id, gallery_name);return false;
	$(".loading").show();
	$.ajax({
		type: "POST",
		url: "'.$globals['site_url'].'?act=gallery&api=json&delete_gallery=1",
		data: {"gallery_id": gallery_id, "gallery_name":gallery_name},
		dataType: "json",
		success: function(data){
			$(".loading").hide();

			// console.log("galler", $(ele).data("gallery_id"));
			// console.log(data);return false;
			if (data.error !== null || data.error !== undefined){
				$("#div_gallery_"+gallery_id).remove();
				alert(data.done.msg);
			}else{
				alert(data.error.join("\n"));
				// alert("something went wrong");
			}
		},
		Error: function(error){
			alert("something went wrong");
		}
	});
}

$(document).ready(function(){
	$("#exampleModal").on("show.bs.modal", function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var gellery_id = button.data("gallery_id") // Extract info from data-* attributes
	  var inst_id = button.data("insta_id");
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal\'s content. We\'ll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find(".modal-body #insta_id").val(inst_id);
	  modal.find(".modal-body #comment_blog_id").val(gellery_id);
	});

	// Gets the video src from the data-src on each button
	var imageSrc;  
	$(".gallery img").click(function() {
	    imageSrc = $(this).attr("src");
	});

	// when the modal is opened autoplay it  
	$("#imagemodal").on("shown.bs.modal", function (e) {
	    
	// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you\'re gonna get

		$("#image").attr("src", imageSrc  ); 
	})

	// reset the modal image
	$("#myModal").on("hide.bs.modal", function (e) {
	    // a poor man\'s stop video
	    $("#image").attr("src",""); 
	});
});

</script>
';
}
	ss_footer();

}