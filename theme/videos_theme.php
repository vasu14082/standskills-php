<?php

function videos_theme(){

global $globals, $theme, $done, $error, $addvideo, $videos;

	ss_header(STANDSKILLS.' - Videos');
	ss_navigation('videos');

echo '
<style>
.fullyt:hover .yt {
	// filter:  blur(0px);
}

.yt {
	// filter: blur(6px)
}

</style>
';

if(isadmin()){

crate_button($globals['site_url'].'?act=videos&addvideo=1');

}

loader();

error_handle_with_dismiss($error);

done_handle_with_dismiss($done);

if(!empty($addvideo)){

	echo '
	<h1>Create video</h1><br>
		<form method="post" id="addvideo">
			<div class="row form-group">
		        <label for="video_title" class="col-sm-3">Add Video Title</label>
		        <input type="text" id="video_title" class="form-control col-sm-7" name="video_title" value="'.aPOSTval('video_title', '').'" />
		    </div>
			<div class="row form-group">
		        <label for="video_author" class="col-sm-3">Author</label>
		        <input type="text" id="video_author" class="form-control col-sm-7" name="video_author" value="'.aPOSTval('video_author', '').'" />
		    </div>
		    <div class="row form-group">
		        <label for="videos_link" class="col-sm-3">Paste youtube Video Link (*)</label>
		        <input type="text" id="videos_link" class="form-control col-sm-7" name="videos_link" value="'.aPOSTval('videos_link', '').'">
		    </div>
		    <div class="row form-group justify-content-center">
		        <input type="submit" name="create_video" class="btn btn-primary" value="Create Video">&nbsp;&nbsp;
		        <a class="btn btn-danger" href="'.$globals['site_url'].'?act=videos">
		            Cancel
		        </a>
		    </div>
		</form>
<script>

$(document).ready(function(){
	$("#addvideo").submit(function(e){
		let videos_link = $("#videos_link").val();
		let error_list = [];
  		$("#error").html("");
		let url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		if(!url_validate.test(videos_link)){
			error_list.push("Please enter correct video link");
		}
		var str = error_hand_with_dismiss(error_list);
	   $("#error").html(str).show();

		if(error_list.length > 0){
			return false;
		}
	})
})
</script>';
}else{


echo '
<div class="row">
	<div class="col-sm-12 text-center">
		<h1>Our Videos Gallery</h1>
	</div>
</div>
<nav class="navbar navbar-light bg-light">
	<div class="input-group">
	  <div class="input-group-prepend col-sm-3">
	    <span class="input-group-text" id="basic-addon1">@</span>
	  </div>
	  <input type="text" class="form-control col-sm-6" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
	  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	</div>
</nav>
<div class="row" id="video_container" data-page="0">';

/*foreach ($videos['data'] as $key => $value) { 		 	

	if($value['video_type'] != 'youtube'){
		continue;
	}
	echo '<div class="col-sm-6 fullyt" style="margin-top:10px">
		<div class="card">
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item yt" width="100%" height="100%" src="https://www.youtube.com/embed/'.$value['video_link'].'?rel=0&amp;mute=1" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="card-body">
			    <h5 class="card-title">'.$value['video_author'].'</h5>
			    <p class="card-text">'.$value['video_title'].'</p>
			    <a href="https://youtu.be/'.$value['video_link'].'" target="_blank" class="btn btn-primary"><i class="fab fa-youtube"></i> View Video</a>
			 </div>
		</div>
	</div>';
}*/
	echo'
</div>
<div class="row justify-content-center">
	<button type="button" class="btn btn-primary" id="loadmorevideo" onclick="onloadmorevideos()">Load more Videos</button>
</div>
';

echo '
<script>
function make_videos_html(data = []){
	var str = "";

	$.each(data, function(key, value){
		str += \'<div class="col-sm-6 fullyt" style="margin-top:10px"><div class="card"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item yt" width="100%" height="100%" src="https://www.youtube.com/embed/\'+value["video_link"]+\'?rel=0&amp;mute=1&enablejsapi=1" frameborder="0" allowfullscreen></iframe></div>\';

		str += \'<div class="card-body"><h5 class="card-title">\'+value["video_author"]+\'</h5><p class="card-text">\'+value["video_title"]+\'</p><a href="https://youtu.be/\'+value["video_link"]+\'" target="_blank" class="btn btn-primary"><i class="fab fa-youtube"></i> View Video</a></div></div></div>\';
	});

	return str;
}

function onloadmorevideos(){
	let page = $("#video_container").data("page");
	video_container(page);
}

function video_container(page = 0){
	$(".loading").show();
	// console.log("'.$globals['site_url'].'?act=videos&api=json");return false;
	$.ajax({
		type: "GET",
		url: "'.$globals['site_url'].'?act=videos&api=json&page="+page,
		// data: data,
		dataType: "json",
		// context: this,
		success: function(data){
			console.log(data);
			$(".loading").hide();
			let str = make_videos_html(data.videos.data);
			// console.log(str);
			$("#video_container").append(str);
			let totalfetch = parseInt(data.videos.page)+parseInt(data.videos.fetched);
			$("#video_container").attr("data-page", totalfetch);
			if(totalfetch >= data.videos.total){
				$("#loadmorevideo").hide();
			}else{
				$("#loadmorevideo").show();
			}
		},
		Error: function(error){
			// console.log(error);
			$(".loading").hide();
			alert("something went wrong");
		}
	});
}
$(document).ready(function(){
	video_container();
	$(".play-video").click(function(){
		console.log("video played");
	});
});
</script>';

}
	ss_footer();

}