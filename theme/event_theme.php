<?php

function event_theme(){

global $globals, $theme, $error, $done, $addevent, $events, $event, $register, $cat_events, $event_p_type, $showtickets, $tickets, $notify_email_list, $show;
	// $error = ["Event Name is required", "Event Address is required", "Event Date is required", "Event Time is required", "Event Image is required"];
	ss_header(env('APP_NAME', 'standskill').' - Events');
	ss_navigation('event');

echo '
<style>

</style>
';

$event_cat = env("event_cat");

if(!empty(isadmin()) && empty($addevent)){
	crate_button($globals['site_url'].'?act=event&addevent=1');
}

loader();

error_handle_with_dismiss($error);

done_handle_with_dismiss($done);

if(!empty($addevent)){

// ss_dump($event_cat);

	echo '
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h1>Create event</h1>
		<form method="post" id="eventform" enctype="multipart/form-data">
			<div class="form-group">
				<label for="event_name"> Event Name : </label>
				<input type="text" name="event_name" id="event_name" class="form-control" value="'.aPOSTval('event_name').'">
			</div>

			<div class="form-group">
				<label for="event_add"> Event Address : </label>
				<input type="text" name="event_add" id="event_add" class="form-control" value="'.aPOSTval('event_add').'">
			</div>

			<div class="form-group">
				<label for="event_date"> Event Date : </label>
				<input type="date" name="event_date" id="event_date" class="form-control" value="'.aPOSTval('event_date').'">
			</div>

			<div class="form-group">
				<label for="event_time"> Event Time : </label>
				<input type="time" name="event_time" id="event_time" class="form-control" value="'.aPOSTval('event_time').'">
			</div>

			<div class="form-group">
		        <label for="event_details">Event Details :</label>
		        <textarea id="event_details" class="form-control" name="event_details" rows="5">'.aPOSTval('event_details').'</textarea>
		    </div>
		    <div class="form-group">
		        <label for="event_image">Shayri image<span class="require">*</span></label>
		        <input type="file" id="event_image" name="event_image" accept="image/*" onchange="loadFile(event)" value="'.aPOSTval('event_image').'">
		    </div>
		    <div class="form-group">
		        <label>Event price (in INR ) :</label>
		        <div class="row">';
		        	foreach($event_cat as $cat => $cv){
		        		echo '<div class="col-sm-12">
		        		<h2>'.$cat.'</h2>
		        		<label for="'.$cat.'">'.ucwords($cat).'</label>
		        		<input type="number" id="'.$cat.'" name="price['.$cat.'][m_cat]">
		        		';

		        		foreach ($cv as $sk => $sv) {
		        			if($sk == 'data'){
		        				continue;
		        			}
		        			echo '<input type="hidden" name="price['.$cat.']['.$sk.']" value="'.$sv.'">';
		        		}

		        		if(!empty($cv)){
		        			echo '
		        			<div class="form-check">
							    <input type="checkbox" class="form-check-input s_a_sub_cat_event" id="checkbox_'.$cat.'" data-cat="'.$cat.'">
							    <label class="form-check-label" for="checkbox_'.$cat.'">Select For all sub category</label>
							    <span style="border:2px solid black" data-toggle="collapse" data-target="#'.$cat.'_sub">show sub category</span>
							</div>
		        			';

		        			echo '<div id="'.$cat.'_sub" class="collapse hide">
		        				<ul class="list-group">';

		        			foreach ($cv['data'] as $v){
		        				echo '<li class="list-group-item">'.ucwords($v).': <input type="number" name="price['.$cat.'][data]['.$v.']" value="'.aPOSTval('price['.$cat.'][data]['.$v.']').'"></li>';
		        			}

		        			echo '</ul>
		        			</div>';
		        		}
		        		echo '
		        		</div>';
		        	}
		        echo '
		        </div>
		    </div>
		    <div class="form-group">
		        <input type="submit" name="create_event" class="btn btn-primary" value="Create event">
		        <a class="btn btn-danger" href="'.$globals['site_url'].'?act=event"> Cancel </a>
		    </div>
		    <div class="form-group row">
		    	<div class="col-sm-12">
		    		<image id="preview" width="100%">
		    	</div>
		    </div>
		</form>
	</div>
</div>
<script>
 var loadFile = function(event){
    var output = document.getElementById("preview");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };

$(document).ready(function(){
	$("#eventform").submit(function(e){
		// e.preventDefault();
		var error_list = [];
		$("#error").html("");
		let event_name = $("#event_name").val().trim();
		let event_add = $("#event_add").val().trim();
		let event_date = $("#event_date").val().trim();
		let event_time = $("#event_time").val().trim();
		let event_details = $("#event_details").val().trim();
		let event_image = $("#event_image")[0].files;
		console.log(event_date, event_time, Date.now(),new Date(event_date));
		if(!event_name){
			error_list.push("Event Name is required");
		}
		if(!event_add){
			error_list.push("Event Address is required");
		}
		if(!event_date){
			error_list.push("Event Date is required");
		}
		if(!event_time){
			error_list.push("Event Time is required");
		}

		if(event_image.length < 1){
			error_list.push("Event Image is required");
		}

		// if(new Date(event_date) < Date.now()){
		// 	error_list.push("The Date must be Bigger or Equal to today date "+Date.now().toString());
		// }

		var currtime = new Date().getTime();

		var etime = new Date(event_date+" "+event_time).getTime();


	  	if(etime <= currtime){
	  		error_list.push("Event Date and Time will be greater than current date and time");
	  	}

	   var str = error_hand_with_dismiss(error_list);
	   $("#error").html(str).show();

		if(error_list.length > 0){
			return false;
		}
	})

	$(".s_a_sub_cat_event").click(function(){
		var cat = $(this).data("cat");
		if($(this).is(":checked")){
			$("#"+cat+"_sub :input").each(function(){
				$(this).val($("#"+cat).val());
			});
		}else{
			$("#"+cat+"_sub :input").each(function(){
				$(this).val("");
			});
		}
	});
});
</script>';	

}elseif(!empty($register) && is_array($event)){
	$c_event = json_decode($event['event_price'], true);
	$f_key = array_key_first($c_event);
// ss_print($c_event);
echo '
<div class="ssbg">
<div class="row">
	<div class="title col-sm-12">
		<h1>'.$event['event_name'].'</h1>
	</div>
</div>
';

echo '
<div class="row" style="display:none">
	<div class="col-md-12 bg-light text-right">
        <button type="button" class="btn btn-info">Show Booked Ticket</button>
    </div>
</div>
';

echo '
<div class="row">
	<div class="col-sm-4">
	</div>
	<div class="col-sm-4">
		<img class="img rounded mx-auto d-block img-thumbnail of event_image" data-toggle="modal" data-target="#imagemodal" src="'.$globals['events_path'].'/'.$event['event_image'].'" alt="'.$event['event_name'].'" >
		<div class="share-button text-right">
			'.share_button($globals['events_path'].'/'.$event['event_image'], $event['event_name'], 2).'
		</div>
	</div>
	<div class="col-sm-12">

	</div>
</div><br>
<form id="new_payment_form" method="POST" class="form">
	<input type="hidden" name="payment_type" value="event">
	<input type="hidden" name="csrf_token" value="'.gettoken().'">
	<input type="hidden" name="event_price_fields" value=\''.$event['event_price'].'\' >';

foreach ($c_event as $k => $v) {
	echo '
	<div class="row ssrow" data-cat="'.$k.'" data-ismultipersion="'.(!empty($v['isMultiPerson']) ? 1 : 0).'">
		<div class="col-sm-4">
			<button type="button" class="btn btn-secondary showhide"><i class="fas fa-plus-circle fa-2x" id="'.$k.'_fa_button"> '.$k.' ('.$v['m_cat'].'₹)</i></button>
			<input type="hidden" name="bill['.$k.'][m_cat]" value="'.$v['m_cat'].'" >
		</div>
		<div class="row subrow" id="'.$k.'_showhide" style="display:none" data-cat="'.$k.'">
			<div class="col-sm-5">
				<input type="hidden" name="bill['.$k.'][subname]" id="event_person_type_'.$k.'" value="'.array_key_first($v['data']).'" '.(count($v['data']) < 1 ? 'disabled' : '').'>
				Select Person Type (cat):-
				<select name="bill['.$k.'][sub]" class="form-control sub_type" '.(count($v['data']) < 1 ? 'disabled' : '').'>';
					foreach ($v['data'] as $dk => $dv){
					echo '
					<option value="'.(!empty($dv) ? $dv : $v['m_cat']).'" data-sub="'.$dk.'">'.ucwords($dk).' ( '.(!empty($dv) ? $dv : $v['m_cat']).'₹)</option>
					';
				}
				echo '
				</Select>
			</div>
			<div class="col-sm-5">
				Select Number Of Person:-<input type="number" name="bill['.$k.'][n_per]" id="noper_'.$k.'" class="form-control noper" value="0" min="0">
			</div>
			<div class="col-sm-2">
				<span id="total_'.$k.'_price">0</span>'.env('PAY_SIGN').'
			</div>
		</div>
	</div><br>
	';
}
echo '
<input type="hidden" id="final_event_price" name="final_event_price" value="0">
<button type="submit" name="new_payment_form_submit" class="btn btn-primary">Pay Now ('.env('PAY_SIGN').'<span id="final_event_price_span">0</span>)</button>
</form>
<br />
<!--<div>
	<form id="payment_form" method="POST">
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">Select Person Type</th>
	      <th scope="col">Select Person Type (cat)</th>
	      <th scope="col">Select Number Of Person</th>
	      <th scope="col">Total Price</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<input type="hidden" name="payment_type" value="event">
	  	<input type="hidden" name="csrf_token" value="'.gettoken().'">';

	  	echo '	
	  	<input type="hidden" name="e_f_price_field" value=\''.$event['event_price'].'\' >';
	  	foreach ($c_event as $key => $value){
	  		echo '
	  		<tr data-cat="'.$key.'">
	  			<td>
	  				<input type="hidden" name="bill['.$key.'][m_cat]" value="'.$value['m_cat'].'" >
	  				'.ucwords($key).' ('.$value['m_cat'].'₹)
	  			</td>
	  			<td>
	  				<input type="hidden" name="bill['.$key.'][subname]" id="es_'.$key.'" value="" '.(count($value) <= 1 ? 'disabled' : '').'>
	  				<select name="'.$key.'[sub]" class="form-control e_s_type" '.(count($value) <= 1 ? 'disabled' : '').'>';
	  					foreach ($value as $k => $v){
							if($k == 'm_cat') continue;
							echo '
							<option value="'.(!empty($v) ? $v : $value['m_cat']).'" data-sub="'.$k.'">'.ucwords($k).' ( '.(!empty($v) ? $v : $value['m_cat']).'₹)</option>
							';
						}
	  				echo '
	  				</Select>
	  			</td>
	  			<td>
	  				<input type="number" name="bill['.$key.'][n_per]" id="nper_'.$key.'" class="form-control n_per" value="0">
	  			</td>
	  			<td><span id="total_'.$key.'">0</span> ₹</td>
	  		</tr>
	  		';
	  	}
	  	echo '
	  </tbody>
	</table>
	<div class="form-group row justify-content-center" class="btnpaynow">
		<input type="hidden" id="e_f_price_val" name="e_f_price_val" value="0">
		<input type="submit" class="btn btn-primary" name="payment_form" value="Pay Now"> (&#8377;<span id="e_f_price">0</span>)
	</div>
	</form>
</div> -->
</div>
';
echo '
<script>
$(document).ready(function(){
	var event_price = '.$event['event_price'].';
	// console.log(event_price);
	$(".showhide").click(function(){
		var row = $(this).closest("div.ssrow");
		$("#"+row.data("cat")+"_showhide").toggle("slow", "swing", function(){
			stateoftogle(row.data("cat"), $(this).is(":visible"));
		});
		// console.log(row.data("cat"));
	});

	function stateoftogle(cat, visiblestate){
		// console.log(cat, visiblestate);
		if(visiblestate){
			$("#"+cat+"_fa_button").attr("class", "fas fa-minus-circle fa-2x")
		}else{
			$("#noper_"+cat).val(0);
			$("#total_"+cat+"_price").html(0);
			settotalpaymentnew();
			$("#"+cat+"_fa_button").attr("class", "fas fa-plus-circle fa-2x")
		}
	}

	$(".sub_type").click(function(){
		var selectedele = $(this).find("option:selected");
		var divele = $(this).closest("div.subrow");
		var cat = divele.data("cat");
		$("#event_person_type_"+divele.data("cat")).val(selectedele.data("sub"));
		// console.log(selectedele.val(), selectedele.data("sub"));
		var noper = $("#noper_"+cat).val();
		// console.log(noper, selectedele.val());
		$("#total_"+cat+"_price").html(noper*selectedele.val());
		settotalpaymentnew();

	});

	$(".noper").click(function(){
		var mainEle = $(this).closest("div.ssrow"), price, cat;
		cat = mainEle.data("cat");
		var subEle = $(this).closest("div.subrow");
		var inputval = parseInt($(this).val());
		var isdisabled = subEle.find(".sub_type").prop("disabled");
		if(isdisabled){
			price = event_price[cat]["m_cat"];
		}else{
			price = subEle.find(".sub_type option:selected").val();
		}

		// console.log(mainEle.data("ismultipersion"));
		if(!mainEle.data("ismultipersion") && inputval>1){
			alert("Multiple Persion is not allowed for "+cat);
			$(this).val(1);
			return false;
		}
		$("#total_"+cat+"_price").html(price*inputval);
		settotalpaymentnew()
	});

	$("#new_payment_form").submit(function(e){
		// e.preventDefault();
		var error_list = [];
		$("#error").html("");

		if(islogin === 0){
			let url = "'.$globals['site_url'].'?act=event&register=1&event_id='.$event['event_id'].'";
			url = encodeURIComponent(url);
			var error_list = ["please login to register this Event <a href=\''.$globals['site_url'].'?act=login&redirect="+url+"\'> Click Here </a>"];
			let str = error_hand_with_dismiss(error_list);
			$("#error").html(str).show();
			return false;
		}

		var nper = 0;
		$.each(event_price, function(key, value){
			nper += parseInt($("#noper_"+key).val());
		});
		
		if(nper < 1){
			error_list.push("Please select atlest one person");
		}

		// console.log(nper, $("#e_f_price").html());

		var str = error_hand_with_dismiss(error_list);
	   	$("#error").html(str).show();
	   	$("#final_event_price").val(parseInt($("#final_event_price_span").html()));

		if(error_list.length > 0){
			return false;
		}

	});

	function settotalpaymentnew(){
		var price = 0;
		$.each(event_price, function(key, value){
			price += parseInt($("#total_"+key+"_price").html());
		});
		$("#final_event_price_span").html(price);
	}


	$(".n_per").change(function(){
		var cat, inputval, price;
		inputval = parseInt($(this).val());
		var trele = $(this).closest("tr");
		cat = trele.data("cat");
		var isdisabled = trele.find("td .e_s_type").prop("disabled");

		if(isdisabled){
			price = event_price[cat]["m_cat"];
		}else{
			price = trele.find("td .e_s_type option:selected").val();
		}

		if(cat === "performer" && inputval > 1){
			alert("you can set only one performer");
			$(this).val(1);
			return false;
		}

		$("#total_"+cat).html(price*inputval);
		settotalpayment();
		console.log(cat, isdisabled, inputval, event_price, price);
		// var price = parseInt($("#e_f_price_hidden").val())*parseInt($(this).val());
		// $("#e_f_price").html(price);
	});

	$(".e_s_type").change(function(){
		var selectedval = $(this).find("option:selected").val();
		var cat, price, trele, n_per; 

		trele = $(this).closest("tr");
		cat = trele.data("cat");
		n_per = trele.find(".n_per").val();
		$("#total_"+cat).html(n_per*selectedval);
		settotalpayment();
	});

	function settotalpayment(){
		var price = 0;
		$.each(event_price, function(key, value){
			price += parseInt($("#total_"+key).html());
		});
		$("#e_f_price").html(price);
	}

	$("#payment_form").submit(function(e){

		// e.preventDefault();
		var error_list = [];
		$("#error").html("");

		if(islogin === 0){
			let url = "'.$globals['site_url'].'?act=event&register=1&event_id='.$event['event_id'].'";
			url = encodeURIComponent(url);
			var error_list = ["please login to register this Event <a href=\''.$globals['site_url'].'?act=login&redirect="+url+"\'> Click Here </a>"];
			let str = error_hand_with_dismiss(error_list);
			$("#error").html(str).show();
			return false;
		}

		var nper = 0;
		$.each(event_price, function(key, value){
			nper += parseInt($("#nper_"+key).val());
			let trele = $("#nper_"+key).closest("tr");
			let isdisabled = trele.find("td .e_s_type").prop("disabled");
			let name="";
			if(!isdisabled){
				name = trele.find("td .e_s_type option:selected").data("sub");
			}
			$("#es_"+key).val(name);
		});
		
		if(nper < 1){
			error_list.push("Please select atlest one person");
		}

		// console.log(nper, $("#e_f_price").html());

		var str = error_hand_with_dismiss(error_list);
	   	$("#error").html(str).show();
	   	$("#e_f_price_val").val(parseInt($("#e_f_price").html()));

		if(error_list.length > 0){
			return false;
		}
	});
});
</script>
';
}elseif(!empty($showtickets) && is_array($event)){
	// ss_print($tickets);

echo '
<h1> Your booked ticket for '.$event['event_name'].'</h1>
';

	foreach ($tickets as $key => $value) {
	echo '
<div class="row">
	<div class="col-sm-6 text-center"><pre>
		Payment Id : '.$value['payment_id'].'
		Payment Type: '.$value['payment_type'].'
		Payment Status: '.$value['payment_status'].'
		Total Ammount: '.$value['payment_amount'].'
		Payment Time: '.ssdatetimeformatter($value['created']).'
		</pre>
	</div>
</div>
	';
	}

}elseif(!empty($show)){
echo '
<div class="row">
	<div class="col-sm-12">
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Email</th>
	      <th scope="col">Is Notify</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>';
	    if(!empty($notify_email_list)){
	    	foreach ($notify_email_list as $key => $value) {
	    		echo '
	    	<tr>
	    		<th scope="row">'.$value['event_notify_id'].'</th>
		        <td>'.$value['email'].'</td>
		      	<td>'.$value['is_notify'].'</td>
		     	 <td>Actions</td>
	    	</tr>
	    	';
	    	}
	    }else{
	    	echo '
	    	<tr>
	    		<td colspan="4">No user yet</td>
	    	</tr>
	    	';
		}
	   echo '
	  </tbody>
	</table>
	</div>
</div>';
} else {
	echo '<br>';
echo '
<style>

.event-back {
	position:relative;
}

.of {
    object-fit: cover;
    /*
    object-fit: scale-down;
    object-fit: contain;
    object-fit: fit;
    */
}
body {
  margin: 20px auto;
  font-family: "Lato";
  background:#666;
  color:#fff;
}

*{
  margin:0;
  padding:0;
}

p {
  font-size: 1rem;
  font-weight: normal;
}

.sticky-notes{
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.eh {
  font-weight: bold;
  font-size: 2rem;
}
.ed {
  font-family: \'Reenie Beanie\';
  font-size: 2rem;
}

.sticky{
	text-decoration:none;
	color:#000;
	background:#ffc;
	display:block;
	padding:1em;
	box-shadow: 5px 5px 7px rgba(33,33,33,.7);
	transform: rotate(-6deg);
}

.sticky-notes:nth-child(even) .sticky{
  transform:rotate(4deg);
  position:relative;
  top:5px;
}

.sticky-notes:nth-child(3n) .sticky{
  transform:rotate(4deg);
  position:relative;
  top:5px;
}

.sticky-notes:nth-child(5n) .sticky{
  transform:rotate(4deg);
  position:relative;
  top:5px;
}

.sticky:focus{
  box-shadow:10px 10px 7px rgba(0,0,0,.7);
  transform: scale(1.25);
  position:relative;
  z-index:5;
}
</style>
<div class="ssbg">';
	
	$_user = islogin();

	foreach ($events as $key => $value){

		echo '
<div class="row">
	<div class="title col-sm-12">
		<h1>'.$value['event_name'].'</h1>
	</div>
</div>
<div class="row justify-content-center">
	';
	if(isadmin()){
		echo '
		<a type="button" href="'.$globals['site_url'].'?act=event&show=email&event_id='.$value['event_id'].'" class="btn btn-secondary col-sm-2">Show Notified User</a>';
	}
	echo '
		<button type="button" class="btn btn-info col-sm-2" data-info=\'{ "event_id":'.$value['event_id'].', "is_notify":'.(!empty($value['is_notify']) ? '0' : '1').' }\' data-event_id="'.$value['event_id'].'" data-is_notify="'.(!empty($value['is_notify']) ? '0' : '1').'" data-email="'.(!empty($_user['email']) ? $_user['email'] : '').'" onclick="toggle_notificaiton(this)"><i class="fa'.(!empty($value['is_notify']) ? 's' : 'r').' fa-bell" id="bell_'.$value['event_id'].'"></i> Interested (Notify me)</button>
		<a type="button" href="'.$globals['site_url'].'?act=event&register=1&event_id='.$value['event_id'].'" class="btn btn-dark col-sm-2">Register</a>
</div>
<div class="row">
	<div class="col-sm-4">
	</div>
	<div class="col-sm-4">
		<img class="img rounded mx-auto d-block img-thumbnail of event_image" data-toggle="modal" data-target="#imagemodal" src="'.$globals['events_path'].'/'.$value['event_image'].'" alt="'.$value['event_name'].'" >
		<div class="share-button text-right">
			'.share_button($globals['events_path'].'/'.$value['event_image'], $value['event_name'], 2).'
		</div>
	</div>
	<div class="col-sm-4">
		<div class="sticky-notes">
			<div class="sticky">
				<h2 class="eh">Event Name</h2>
      			<p class="ed">'.$value['event_name'].'</p>
			</div>
			<div class="sticky">
				<h2 class="eh">Event Address</h2>
      			<p class="ed">'.$value['event_add'].'</p>
			</div>
			<div class="sticky">
				<h2 class="eh">Event Details</h2>
      			<p class="ed">'.$value['event_details'].'</p>
			</div>
			<div class="sticky">
				<h2 class="eh">Event DataTime</h2>
      			<p class="ed">'.ssdatetimeformatter($value['event_date'].' '.$value['event_time']).'</p>
			</div>	
		</div>
	</div>
</div>
';
	}

echo '</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog md-dialog" role="document">
    <div class="modal-content">

      
      <div class="modal-body md-body">

       <button type="button" class="close cls" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        

                <img src="//media.tenor.com/images/556e9ff845b7dd0c62dcdbbb00babb4b/tenor.gif" alt="" height="100%" width="100%" id="image" class="img-fluid">

        
        
      </div>

    </div>
  </div>
</div>
';

echo '
<!-- Modal -->
<div class="modal fade" id="notifymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"><form id="notify_form">
      <div class="modal-header">
        <h5 class="modal-title text-info" id="exampleModalLongTitle">Enter your email we will notify you event details on your mail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-secondary">
        Email:<input type="email" class="form-control" name="notify_email" id="notify_email" required>
        <label class="text-danger"></label>
        <input type="hidden" id="event_json">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Notify Me</button>
      </div></form>
    </div>
  </div>
</div>
';

}

echo '
<script>
function toggle_notificaiton(ele){
	var data = {"event_id": $(ele).data("event_id"), "is_notify": $(ele).data("is_notify"), "email": $(ele).data("email")};
	
	$("#notifymodal").modal("show");
	var modal = $("#notifymodal");
  	modal.find(".modal-body #notify_email").val(data.email);
  	modal.find(".modal-body #event_json").val(JSON.stringify(data));
  	return true;
	if(islogin === 0){
		/*let url = "'.$globals['site_url'].'?act=event&notify=1&is_notify="+data.is_notify+"&event_id="+data.event_id;
		url = encodeURIComponent(url);
		var error_list = ["please login to notify this Event <a href=\''.$globals['site_url'].'?act=login&redirect="+url+"\'> Click Here </a>"];
		let str = error_hand_with_dismiss(error_list);
		$("#error").html(str).show();
		return false;*/

		$("#notifymodal").modal("show");
		return true;

	}
}
$(document).ready(function(){

	// Gets the video src from the data-src on each button
	var imageSrc;  
	$(".event_image").click(function() {
	    imageSrc = $(this).attr("src");
	    console.log(imageSrc);
	});

	// when the modal is opened autoplay it  
	$("#imagemodal").on("shown.bs.modal", function (e) {
	    
	// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you\'re gonna get

		$("#image").attr("src", imageSrc  ); 
	})

	// reset the modal image
	$("#imagemodal").on("hide.bs.modal", function (e) {
	    // a poor man\'s stop video
	    $("#image").attr("src",""); 
	});

	$("#notify_form").submit(function(e){
		e.preventDefault();
		var data = JSON.parse($("#event_json").val());
		var email = $("#notify_email").val();
		if(!isEmail(email)){
			$(this).find(".text-danger").text("please enter valid email");
			return false;
		}
		data.email = email;
		// console.log(data, typeof data);return false;
		$.ajax({
			type: "POST",
			url: "'.$globals['site_url'].'?act=event&api=json&notify=1",
			data: data,
			dataType: "json",
			context: this,
			success: function(data){
				$(".loading").hide();
				// console.log(data);return false;
				if(data.error){
					let str = error_hand_with_dismiss(data.error);
					$(this).find(".text-danger").html(str);
				}else{
					let str = done_handler_with_dismiss_js(data.done);
					console.log(str)
					$("#done").html(str).show();
					$("#notifymodal").modal("hide");
				}
			},
			Error: function(error){
				alert("something went wrong");
			}
		});
	});
});
</script>
';


ss_footer();

}

function get_first_value($arr = []){
	if(array_key_exists('m_cat', $arr)){
		unset($arr['m_cat']);
	}

	return array_key_first($arr);
}