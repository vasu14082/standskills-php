<?php

function addpost_theme(){

global $globals, $theme, $error, $done;
	ss_header(STANDSKILLS.' - Add Post');
	ss_navigation('addpost');

	echo '
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h1>Create post</h1>
		<form method="post" id="shayriform" enctype="multipart/form-data">
			<div class="row">
				<ul class="col-sm-12 alert alert-danger" style="display:'.(!empty($error) ? '': 'none').'" role="alert" id="error">
					'.error_handle($error).'
				</ul>';
				if(!empty($done)){

					echo '<ul class="col-sm-12 alert alert-success" style="display:'.(!empty($done) ? '': 'none').'" role="alert" id="done">
					<li>'.$done['msg'].'</li>
				</ul>';
				}
			echo '
			</div>
			<div class="form-group">
		        <label for="shayri">Shayri</label>
		        <textarea id="shayri" class="form-control" name="shayri" rows="5"></textarea>
		    </div>
		    <div class="form-group">
		        <label for="shayri_image">Shayri image<span class="require">*</span></label>
		        <input type="file" id="shayri_image" name="shayri_image" accept="image/*" onchange="loadFile(event)">
		    </div>
		    <div class="form-group">
		    	<image id="preview" width="100" height="100">
		    </div>
		    <div class="form-group">
		        <input type="submit" name="create_shayri" class="btn btn-primary" value="Create">
		        <button class="btn btn-danger" onclick="javascript:history.back()">
		            Cancel
		        </button>
		    </div>
		</form>
	</div>
</div>
<script>
 var loadFile = function(event){
    var output = document.getElementById("preview");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };

  $("#shayriform").submit(function(e){
  	let shayri = $("#shayri").val();
  	let shayri_image = $("#shayri_image")[0].files;
  	if(shayri_image.length < 1 && shayri.trim().length < 1){
  		$("#error").append("<li>Please enter shayri or shayri image</li>").show();
  	}
  	if($("#error").html().trim().length > 1){
		return false;
	}
  });
</script>
';
	ss_footer();

}