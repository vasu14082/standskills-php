<?php

function verify_theme(){

global $globals, $theme, $error, $done;

	ss_header(STANDSKILLS.' - verify');
	
	if(!empty($done)){
		echo '
		<div class="row">
			your account is verified <a href="'.$globals['site_url'].'?act=login">click here to login</a>
		</div>
		';
	}else{
		
		echo '
		<div class="row">
			<ul class="col-sm-12 alert alert-danger" style="display:'.(!empty($error) ? '': 'none').'" role="alert" id="error">
				'.error_handle($error).'
			</ul>
		</div>
		';
	}

	ss_footer();

}