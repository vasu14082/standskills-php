<?php

function register_theme(){

global $globals, $theme, $error, $done;

	ss_header(env('APP_NAME').' - Register');
echo '
<style>
.main-content{
	width: 50%;
	border-radius: 20px;
	box-shadow: 0 5px 5px rgba(0,0,0,.4);
	margin: 5em auto;
	display: flex;
}
.company__info{
	background-color: #008080;
	border-top-left-radius: 20px;
	border-bottom-left-radius: 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	color: #fff;
}
.fa-android{
	font-size:3em;
}
@media screen and (max-width: 640px) {
	.main-content{width: 90%;}
	.company__info{
		display: none;
	}
	.login_form{
		border-top-left-radius:20px;
		border-bottom-left-radius:20px;
	}
}
@media screen and (min-width: 642px) and (max-width:800px){
	.main-content{width: 70%;}
}
.row > h2{
	color:#008080;
}
.login_form{
	background-color: #fff;
	border-top-right-radius:20px;
	border-bottom-right-radius:20px;
	border-top:1px solid #ccc;
	border-right:1px solid #ccc;
}
form{
	padding: 0 2em;
}
.form__input{
	width: 100%;
	border:0px solid transparent;
	border-radius: 0;
	border-bottom: 1px solid #aaa;
	padding: 1em .5em .5em;
	padding-left: 2em;
	outline:none;
	margin:1.5em auto;
	transition: all .5s ease;
}
.form__input:focus{
	border-bottom-color: #008080;
	box-shadow: 0 0 5px rgba(0,80,80,.4); 
	border-radius: 4px;
}
.btn{
	transition: all .5s ease;
	width: 70%;
	border-radius: 30px;
	color:#008080;
	font-weight: 600;
	background-color: #fff;
	border: 1px solid #008080;
	margin-top: 1.5em;
	margin-bottom: 1em;
}
.btn:hover, .btn:focus{
	background-color: #008080;
	color:#fff;
}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}

</style>
';

echo '
<div class="row">
	<ul class="col-sm-12 alert alert-danger" style="display:'.(!empty($error) ? '': 'none').'" role="alert" id="error">
		'.error_handle($error).'
	</ul>
</div>';

echo '
<div class="row">
	<ul class="col-sm-12 alert alert-success" style="display:'.(!empty($done) ? '': 'none').'" role="alert" id="done">
		'.$done['msg'].'
	</ul>
</div>';

echo '
<div class="row main-content bg-success text-center">
	<div class="col-md-4 text-center company__info" onclick="window.location.href=\''.$globals['site_url'].'\'">
		<span class="company__logo"><h2><span class="fa fa-android"></span></h2></span>
		<h4 class="company_title">STANDSKILLS</h4>
	</div>
	<div class="col-md-8 col-xs-12 col-sm-12 login_form ">
		<div class="container-fluid">
			<div class="row">
				<h2>Register</h2>
			</div>
			<div class="row">
				<form control="" method="post" class="form-group" id="user_register">

					<div class="row">
						<input type="text" name="user" id="user" class="form__input" placeholder="User Name">
					</div>
					<div class="row">
						<input type="email" name="email" id="email" class="form__input" placeholder="Email">
					</div>
					<div class="row">
						<input type="number" name="mobile_no" id="mobile_no" class="form__input" placeholder="Mobile No">
					</div>
					<div class="row">
						<!-- <span class="fa fa-lock"></span> -->
						<input type="password" name="password" id="password" class="form__input" placeholder="Password">
					</div>
					<div class="row">
						<input type="password" id="cnf_password" name="cnf_password" placeholder="Confirm Password" class="form__input">
					</div>
					<div class="row">
						<input type="submit" value="Register" name="register_user" class="btn">
					</div>
				</form>
			</div>
			<div class="row">
				<p> have an account? <a href="'.$globals['site_url'].'?act=login">Login Here</a></p><br>
			</div>
		</div>
	</div>
</div>

';

echo '
<!-- <div class="row justify-content-center">
<div class="col-sm-6">
	<form method="post" id="user_register">
		<h1>Register</h1>
		<div class="form-group row">
	    	<label for="user" class="col-sm-4">User Name</label>
	    	<input type="text" class="form-control col-sm-8" id="user" name="user">
	  	</div>
		<div class="form-group row">
	    	<label for="email" class="col-sm-4">Email address</label>
	    	<input type="email" class="form-control col-sm-8" id="email" name="email">
	  	</div>
	  	<div class="form-group row">
	    	<label for="mobile_no" class="col-sm-4">Mobile Number</label>
	    	<input type="number" class="form-control col-sm-8" id="mobile_no" name="mobile_no" maxlength="10" >
	  	</div>
	  	<div class="form-group row">
	    	<label for="password" class="col-sm-4">Password</label>
	    	<input type="password" class="form-control col-sm-8" id="password" name="password">
	  	</div>
	  	<div class="form-group row">
	    	<label for="cnf_password" class="col-sm-4">Confirm password</label>
	    	<input type="password" class="form-control col-sm-8" id="cnf_password" name="cnf_password" pa>
	  	</div>
	  	<input type="submit" name="register_user" class="btn btn-primary" value="Register">? Have an Account <a href="'.$globals['site_url'].'?act=login">here to Login</a>
  </form>
</div>
</div> -->
<script>

$(document).ready(function(){
	$("#user_register").submit(function(e){
		// e.preventDefault();
		$("#error").html("");
		let email = $("#email").val();
		let password = $("#password").val();
		let cnf_password = $("#cnf_password").val();
		let mobile_no = $("#mobile_no").val();
		let user = $("#user").val();

		// console.log(email, user, password, mobile_no);

		if(!user){
			$("#error").append("<li>User is not valid</li>").show();	
		}
		if(!isEmail(email)){
			$("#error").append("<li>Email is not valid</li>").show();
		}
		if(!password){
			$("#error").append("<li>Please enter password</li>").show();
		}
		if(!cnf_password){
			$("#error").append("<li>Please enter confirmation password</li>").show();
		}
		if(password !== cnf_password){
			$("#error").append("<li>password and Confirm password doesn\'t match</li>").show();
		}
		if(password.length < 8){
			$("#error").append("<li>Your password should be atleast 8 digit</li>").show();	
		}
		if(mobile_no.length !== 10 || !mobile_no){
			$("#error").append("<li>mobile number is not valid</li>").show();	
		}
		if($("#error").html().trim().length > 1){
			return false;
		}

		// return false;
	});
});
</script>
';

	ss_footer();

}