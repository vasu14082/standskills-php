<?php

function blog_theme(){

global $globals, $theme, $error, $done, $addblog, $blogs, $_blog;

ss_header(STANDSKILLS.' - Write Blog/Script');

ss_navigation('blog');



if(empty($addblog) && !empty(islogin())){
	crate_button($globals['site_url'].'?act=blog&addblog=1');
}

error_handle_with_dismiss($error);

done_handle_with_dismiss($done);

loader();

if(!empty($addblog) && !empty(islogin())){
	echo '
<div class="row">
	<div class="col-md-8">
		<h1>Create Blog/Script</h1>
		<form method="post" id="blogform" enctype="multipart/form-data">
			<div class="form-group">
		        <label for="blog_title">Write Blog/Script</label>
		        <input type="text" id="blog_title" class="form-control" name="blog_title" '.aPOSTval('blog_title', '').' />
		    </div>
			<div class="form-group">
		        <label for="blog">Write Blog/Script</label>
		        <textarea id="blog" style="resize: none;" class="form-control" name="blog" rows="10">'.aPOSTval('blog', '').'</textarea>
		    </div>
		    <div class="form-group">
		        <label for="blog_image"> Add Blog/Script Image</label>
		        <input type="file" id="blog_image" name="blog_image" accept="image/*" onchange="loadFile(event)">
		    </div>
		    <div class="form-group">
		        <input type="submit" name="create_blog" class="btn btn-primary" value="Create">
		        <a class="btn btn-danger" href="'.$globals['site_url'].'?act=blog'.'">
		            Cancel
		        </a>
		    </div>
		    <div class="form-group">
		    	<image id="preview">
		    </div>
		</form>
	</div>
</div>';
}else if(!empty($_blog)){

create_blog_css();

$is_like = (!empty($_blog['likes_blog']['is_like']) ? 1 : 0);
	echo'
<div class="container pb50">
    <div class="row">
        <div class="col-md-9 mb40">
            <article>
                <img style="border:2px solid black" src="'.(!empty($_blog['blog_image']) ? $globals['upload_path'].'/blogs/'.$_blog['blog_image'] : $globals['asset_loc'].'/img/no-image.png').'" alt="'.$_blog['blog_title'].'" class="img-fluid mb30" width="100%" height="480px">
                <div class="post-content">
                    <h3>'.$_blog['blog_title'].'</h3>
                    <ul class="post-meta list-inline">
                        <li class="list-inline-item">
                            <i class="fa fa-user-circle-o"></i> <a href="#">'.$_blog['user_name'].'</a>
                        </li>
                        <li class="list-inline-item">
                            <i class="fa fa-calendar-o"></i> <a href="#">'.ssdatetimeformatter($_blog['created']).'</a>
                        </li>
                    </ul>
                    <p><pre style="white-space: pre-wrap; word-break: break-word;">'.$_blog['blog_detail'].'</pre></p>
                    <hr class="mb40">
                    Created By <span class="font-weight-bold"><i class="fa fa-user">'.$_blog['user_name'].'</i></span>
                    <span class="share-buttons float-right">Share Blog:'.share_button($globals['site_url'].'?act=blog&blog='.$_blog['blog_id']).'</span>

                    <hr class="mb40">
             
                    <h4 class="mb40 text-uppercase font500">Comments (<span id="total_comment">'.$_blog['comments']['total'].'</span>)<button class="float-right" is_like='.(!empty($is_like) ? 0 : 1).' data-blog_id="'.$_blog["blog_id"].'" onclick="toggle_blog_like(this, 1)"><i class="fa'.(!empty($is_like) ? 's' : 'r').' fa-heart fa-1x custom"></i><span>'.$_blog["likes_count"].'</span></button><button class="float-right" data-toggle="modal" data-target="#exampleModal" data-whatever="'.$_blog['blog_id'].'" data-title="'.$_blog['blog_title'].'"><i class="fas fa-plus-circle"></i> Comment</button></h4>
                    <div id="comments_container" data-page="0">';

                    /*foreach ($_blog['comments'] as $key => $value) {
                    	echo '
                    <div class="media mb40">
                        
                        <div class="media-body">
                            <h5 class="mt-0 font400 clearfix">
                                        <!-- <a href="#" class="float-right">Reply</a>-->
                                        <i class="d-flex mr-3 fa fa-user">'.$value['user_name'].'</i></h5> '.$value['comment'].'
                        </div>
                    </div>

                    	';
                    }*/
                    echo '
                    </div>
                    <button class="btn btn-primary" style="cursor: pointer;" id="loadmorecomment" onclick="onloadmorecomment()"> Load more comment</button>
                </div>
            </article>
            <!-- post article-->

        </div>
	</div>
</div>';
echo '
<script>
function make_comment_html(data = []){
	var str = "";
	if(data.length == 0){
		return str;
	}

	$.each(data, function(key, value){
		str += \'<div class="media mb40">\';
		str += \' <h5 class="mt-0 font400 clearfix"><i class="d-flex mr-3 fa fa-user">\'+value["user_name"]+\'</i></h5> \'+value["comment"];
		str += \'</div></div>\';
	})

	return str;
}

function onloadmorecomment(){
	let page = $("#comments_container").attr("data-page");
	// console.log(page);
	comment_container(page);
}

function comment_container(page = 0){
	$(".loading").show();
	// console.log("'.$globals['site_url'].'?act=videos&api=json");return false;
	$.ajax({
		type: "GET",
		url: "'.$globals['site_url'].'?act=blog&blog='.optGET('blog').'&api=json&page="+page,
		// data: data,
		dataType: "json",
		// context: this,
		success: function(data){
			$(".loading").hide();
			console.log(data);
			let str = make_comment_html(data._blog.comments.data);
			// console.log(str);
			$("#comments_container").append(str);
			let totalfetch = parseInt(data._blog.comments.page)+parseInt(data._blog.comments.fetched);
			$("#comments_container").attr("data-page", totalfetch);
			$("#total_comment").html(data._blog.comments.total);
			if(totalfetch >= data._blog.comments.total){
				$("#loadmorecomment").hide();
			}else{
				$("#loadmorecomment").show();
			}
		},
		Error: function(error){
			// console.log(error);
			$(".loading").hide();
			alert("something went wrong");
		}
	});
}

$(document).ready(function(){
	comment_container();
})
</script>';

}else{

	list_blog_css();

	echo '<section class="sschromebg">
<div class="container">
    <div class="text-center mb-5">
        <!-- <h5 class="text-primary h6">STANDSKILLS Blogs</h5> -->
        <h1 class="display-20 display-md-18 display-lg-16">Most recent our blog</h2>
    </div>';
	echo '
	<div class="row" id="blog_list" data-page="0">';
	echo '
	</div>
	<div class="row justify-content-center" style="margin-top:10px;">
		<button class="btn btn-secondary" onclick="onloadmoreblog()" id="loadmoreblog" >Load More Blog</button>
	</div>';

echo '</div>
</section>
<script>
function make_blog_html(data = []){

	var tmphtml = "";

	$.each(data, function(key, value){
		if(typeof value !== "object"){
			return;
		}
		// console.log(key,value);
		tmphtml += \'<div class="col-lg-4 col-md-6" style="margin:10px 0px"><article class="card card-style2">\';
		var globalupload = "'.$globals['upload_path'].'";
		var asset_loc = "'.$globals['asset_loc'].'";

		tmphtml += \'<div class="card-header"><blockquote class="blockquote mb-0"><p><pre style="white-space: pre-wrap; word-break: break-word;"> <i class="fas fa-user"></i> \'+value["user_name"]+\'</pre></p><footer class="blockquote-footer">\'+value["created"]+\'</footer></blockquote></div>\';

		tmphtml +=\'<div class="card-img"><img class="rounded-top" src="\'+(value["blog_image"] !=="" ? globalupload+"/blogs/"+value["blog_image"] : asset_loc+"/img/no-image.png")+\'" alt="\'+value["blog_title"]+\'" height="280" width="350"></div>\';

		var site_url = "'.$globals['site_url'].'";

		tmphtml += \'<div class="card-body"><h3 class="h5"><a href="\'+site_url+"?act=blog&blog="+value["blog_id"]+\'">\'+value["blog_title"]+\'</a></h3><p class="display-30">\'+(value["blog_detail"].length > 100 ? value["blog_detail"].substring(0, 100)+\'<a href="\'+site_url+"?act=blog&blog="+value["blog_id"]+\'" class="link">...</a>\' : value["blog_detail"])+\'</p><a href="\'+site_url+"?act=blog&blog="+value["blog_id"]+\'" class="read-more">read more</a></div>\';

		let islike = (("likes_blog" in value && value["likes_blog"] !== null && Object.keys(value["likes_blog"]).length !== 0) ? (value["likes_blog"]["is_like"] !== "0" ? 1 : 0) : 0);
		tmphtml += \'<div class="card-footer"><ul><li><a href="#!" is_like=\'+(islike == 1 ? 0 : 1)+\' data-blog_id="\'+value["blog_id"]+\'" onclick="toggle_blog_like(this)"><i class="fa\'+(islike == 1 ? "s" : "r")+\' fa-heart fa-2x custom"></i><span>\'+value["likes_count"]+\'</span></a></li><li><a href="\'+site_url+"?act=blog&blog="+value["blog_id"]+\'"><i class="far fa-comment-dots custom"></i><span>\'+value["comment_count"]+\'</span></a></li></ul></div>\';

		tmphtml += \'</article></div><br/>\';
	});

	return tmphtml;
}

function blog_container(page = 0){
	$(".loading").show();
	// console.log("'.$globals['site_url'].'?act=videos&api=json");return false;
	$.ajax({
		type: "GET",
		url: "'.$globals['site_url'].'?act=blog&api=json&page="+page,
		// data: data,
		dataType: "json",
		// context: this,
		success: function(data){
			$(".loading").hide();
			// console.log(data);return false;
			let str = make_blog_html(data.blogs.data);
			// console.log(str);
			$("#blog_list").append(str);
			let totalfetch = parseInt(data.blogs.page)+parseInt(data.blogs.fetched);
			// console.log(totalfetch);
			$("#blog_list").attr("data-page", totalfetch);
			if(totalfetch >= data.blogs.total){
				$("#loadmoreblog").hide();
			}else{
				$("#loadmoreblog").show();
			}
		},
		Error: function(error){
			// console.log(error);
			$(".loading").hide();
			alert("something went wrong");
		}
	});
}

function onloadmoreblog(){
	let page = $("#blog_list").attr("data-page");
	// console.log(page);
	blog_container(page);
}

$(document).ready(function(){
	blog_container();
});
</script>';
}

echo '<!-- Button trigger modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>';
      if(empty(islogin())){
      	echo '
      		<a class="btn btn-secondary" href="'.$globals['site_url'].'?act=login">Login To Comment</a>
      	';
      }else{
      echo 
	      '<form method="post" id="oncomment">
	      	 <div class="modal-body">
	        
		        	<input type="hidden" name="comment_blog_id" class="form-control" id="comment_blog_id">
		          <div class="form-group">
		            <label for="message-text" class="col-form-label">Comment:</label>
		            <textarea class="form-control" id="blog_comment_text" name="blog_comment_text"></textarea>
		          </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary" name="blog_comment">Comment</button>
		      </div>
	      </form>';
  		}
      echo
    '</div>
  </div>
</div>';

echo '
<script>
 var loadFile = function(event){
    var output = document.getElementById("preview");
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };

function countWords(str){
	str = str.replace(/(^\s*)|(\s*$)/gi,"");
	str = str.replace(/[ ]{2,}/gi," ");
	str = str.replace(/\n /,"\n");
	return str.split("").length;
}

function oncomment(){
	$(".loading").show();
	let comment = $("#blog_comment_text").val();
	let blog_id = $("#comment_blog_id").val();
	// console.log(blog_id, comment);
	$.ajax({
		type: "POST",
		url: "'.$globals['site_url'].'?act=blog&blog_comment=1&api=json",
		data: { "blog_id": blog_id, "blog_comment_text":comment },
		dataType: "json",
		success: function(data){
			$(".loading").hide();
			// console.log("success",data);return;
			if (data.error === null || data.error === undefined){
				// alert(data.done.msg);
				let str = make_comment_html([data._blog_comment]);
				// console.log(str);
				$("#comments_container").prepend(str);
				let totalfetch = parseInt($("#comments_container").data("page"))+1;
				$("#comments_container").attr("data-page", totalfetch);
				$("#total_comment").html(data._blog_comment.total_comment);
				$("#total_comment").html(data._blog_comment.total_comment);
			}else{
				alert(data.error.join("\n"));
			}
			$("#blog_comment_text").val("");
			$("#exampleModal").modal("hide");
		},
		Error: function(error){
			alert("something went wrong");
		}
	});
}
$(document).ready(function(){

	$("#oncomment").submit(function(e){
		e.preventDefault();
		oncomment();
	});

	$("#blogform").submit(function(e){
		let blog = $("#blog").val();
		let blog_title = $("#blog_title").val();
		let blog_image = $("#blog_image")[0].files;
		if(!blog_title){
			$("#error").append("<li>Please enter Blog Title</li>").show();
		}
		if(blog.trim().length < 1){
			$("#error").append("<li>Please enter blog or blog image</li>").show();
		}
		if(countWords(blog) < 100){
			$("#error").append("<li>Pleas add atleas 100 word</li>").show();
		}
		if($("#error").html().trim().length > 1){
			return false;
		}
	})

	$("#exampleModal").on("show.bs.modal", function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data("whatever") // Extract info from data-* attributes
	  var title = button.data("title") 
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal\'s content. We\'ll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find(".modal-title").text("New message to " + title)
	  modal.find(".modal-body #comment_blog_id").val(recipient)
	});
})

function toggle_blog_like(ele, s=2){
	data = {blog_id: $(ele).data("blog_id"), is_like:$(ele).attr("is_like")};
	console.log(data);
	// console.log(data);return false;
	// console.log(data, typeof data);return;
	$.ajax({
		type: "POST",
		url: "'.$globals['site_url'].'?act=blog&blog_like=1&api=json",
		data: data,
		dataType: "json",
		context: ele,
		success: function(data){
			// console.log("success",data);return;
			if (data.error === null || data.error === undefined){
				alert(data.done.msg);
				console.log(data, ele);
				let slug = (data._blog_like.is_like == "0" ? "r" : "s");
				$(ele).find("i").removeAttr("class");
				$(ele).find("i").addClass("fa"+slug+" fa-heart fa-"+s+"x custom");
				$(ele).find("span").html(data._blog_like.likes_count);
				$(ele).attr("is_like", (data._blog_like.is_like == 1 ? 0 : 1));
			}else{
				alert(data.error.join("\n"));
			}
		},
		Error: function(error){
			alert("something went wrong");
		}
	});

}
 </script>';

ss_footer();
}

function list_blog_css(){

	echo '
<style>
body{margin-top:20px;}
.card-style2 {
    position: relative;
    display: flex;
    transition: all 300ms ease;
    border: 1px solid rgba(0, 0, 0, 0.09);
    padding: 0;
    height: 100%;
}
.card-style2 .card-img {
    position: relative;
    display: block;
    background: #ffffff;
    overflow: hidden;
    border-radius: 0.25rem;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}
.card-style2 .card-img img {
    transition: all 0.3s linear 0s;
}
.card-style2:hover .card-img img {
    transform: scale(1.05);
}
// .card-style2 .date {
//     position: absolute;
//     right: 30px;
//     top: 30px;
//     z-index: 1;
//     color: #16bae1;
//     overflow: hidden;
//     padding-bottom: 10px;
//     line-height: 24px;
//     text-align: center;
//     border: 2px solid #ededed;
//     display: inline-block;
//     background-color: #ffffff;
//     text-transform: uppercase;
//     border-radius: 0.25rem;
// }
// .card-style2 .date span {
//     position: relative;
//     color: #ffffff;
//     font-weight: 500;
//     font-size: 20px;
//     display: block;
//     text-align: center;
//     padding: 12px;
//     margin-bottom: 10px;
//     background-color: #00baee;
//     border-radius: 0.25rem;
// }
.card-style2 .card-body {
    position: relative;
    display: block;
    background: #ffffff;
    padding: 2rem;
}
.card-style2 .card-body h3 {
    margin-bottom: 0.8rem;
}
.card-style2 .card-body h3 a {
    color: #004975;
}
.card-style2 .card-body h3 a:hover {
    color: #00baee;
}
.card-style2 .card-footer {
    border-top: 1px solid rgba(0, 0, 0, 0.09);
    background: transparent;
    padding-right: 2rem;
    padding-left: 2rem;
    -ms-flex-align: end;
    align-items: flex-end;
}
.card-style2 .card-footer ul {
    display: flex;
    justify-content: space-between;
    list-style: none;
    margin-bottom: 0;
}
.card-style2 .card-footer ul li {
    font-size: 15px;
}
.card-style2 .card-footer ul li a {
    color: #394952;
}
.card-style2 .card-footer ul li a:hover {
    color: #00baee;
}
.card-style2 .card-footer ul li i {
    color: #00baee;
    font-size: 14px;
    margin-right: 8px;
}

.card-style2 .card-footer ul li .custom {
    color: #00baee;
    font-size: 28px;
    margin-right: 8px;
}

</style>';
}


function create_blog_css(){

echo '
<style>
body{
    margin-top:20px;
}
/*
Blog post entries
*/

.entry-card {
    -webkit-box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.05);
    -moz-box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.05);
    box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.05);
}

.entry-content {
    background-color: #fff;
    padding: 36px 36px 36px 36px;
    border-bottom-left-radius: 6px;
    border-bottom-right-radius: 6px;
}

.entry-content .entry-title a {
    color: #333;
}

.entry-content .entry-title a:hover {
    color: #4782d3;
}

.entry-content .entry-meta span {
    font-size: 12px;
}

.entry-title {
    font-size: .95rem;
    font-weight: 500;
    margin-bottom: 15px;
}

.entry-thumb {
    display: block;
    position: relative;
    overflow: hidden;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
}

.entry-thumb img {
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
}

.entry-thumb .thumb-hover {
    position: absolute;
    width: 100px;
    height: 100px;
    background: rgba(71, 130, 211, 0.85);
    display: block;
    top: 50%;
    left: 50%;
    color: #fff;
    font-size: 40px;
    line-height: 100px;
    border-radius: 50%;
    margin-top: -50px;
    margin-left: -50px;
    text-align: center;
    transform: scale(0);
    -webkit-transform: scale(0);
    opacity: 0;
    transition: all .3s ease-in-out;
    -webkit-transition: all .3s ease-in-out;
}

.entry-thumb:hover .thumb-hover {
    opacity: 1;
    transform: scale(1);
    -webkit-transform: scale(1);
}

.article-post {
    border-bottom: 1px solid #eee;
    padding-bottom: 70px;
}

.article-post .post-thumb {
    display: block;
    position: relative;
    overflow: hidden;
}

.article-post .post-thumb .post-overlay {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.6);
    transition: all .3s;
    -webkit-transition: all .3s;
    opacity: 0;
}

.article-post .post-thumb .post-overlay span {
    width: 100%;
    display: block;
    vertical-align: middle;
    text-align: center;
    transform: translateY(70%);
    -webkit-transform: translateY(70%);
    transition: all .3s;
    -webkit-transition: all .3s;
    height: 100%;
    color: #fff;
}

.article-post .post-thumb:hover .post-overlay {
    opacity: 1;
}

.article-post .post-thumb:hover .post-overlay span {
    transform: translateY(50%);
    -webkit-transform: translateY(50%);
}

.post-content .post-title {
    font-weight: 500;
}

.post-meta {
    padding-top: 15px;
    margin-bottom: 20px;
}

.post-meta li:not(:last-child) {
    margin-right: 10px;
}

.post-meta li a {
    color: #999;
    font-size: 13px;
}

.post-meta li a:hover {
    color: #4782d3;
}

.post-meta li i {
    margin-right: 5px;
}

.post-meta li:after {
    margin-top: -5px;
    content: "/";
    margin-left: 10px;
}

.post-meta li:last-child:after {
    display: none;
}

.post-masonry .masonry-title {
    font-weight: 500;
}

.share-buttons li {
    vertical-align: middle;
}

.share-buttons li a {
    margin-right: 0px;
}

.post-content .fa {
    color: #ddd;
}

.post-content a h2 {
    font-size: 1.5rem;
    color: #333;
    margin-bottom: 0px;
}

.article-post .owl-carousel {
    margin-bottom: 20px !important;
}

.post-masonry h4 {
    text-transform: capitalize;
    font-size: 1rem;
    font-weight: 700;
}
.mb40 {
    margin-bottom: 40px !important;
}
.mb30 {
    margin-bottom: 30px !important;
}
.media-body h5 a {
    color: #555;
}
.categories li a:before {
    content: "\f0da";
    font-family: "FontAwesome";
    margin-right: 5px;
}
</style>
';
}