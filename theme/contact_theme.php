<?php 
 function contact_theme(){
 	 global $globals, $theme, $error, $done;
 	ss_header(env("APP_NAME", STANDSKILLS).' - Contact Us');
	ss_navigation('contact');

	error_handle_with_dismiss($error);

	done_handle_with_dismiss($done);

echo '
<style>
 .info-wrap bg-primary w-100 p-md-5 p-4{
 	height: 250px;
 }

 body {
  background-image: url("'.$globals['asset_loc'].'/img/background3.jpg");
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}

.bg-primary {
    background-color: #00ff55!important;
	}



 .wrapper{
 	width: 1100px;
  padding: 15px;

 	//height:0px;
   
	 box-sizing: initial;
  	}


	.wrapper 
	{
	//border: 1px solid black;
 	// background-color: lightblue;
  	padding: 25px 50px 75px 100px;
	}
	
	.wrapper 
	{
    padding: 6em 0;s
    display: block;
	}


	.dbox w-100 d-flex align-items-start{

    background: #01d28e  ;
	}

	.info-wrap bg-primary w-100 p-md-5 p-4{

		background: #01d28e  ;
	}

	.contact-wrap w-100 p-md-5 p-4
	{

		height:100px;
	}

</style>
';




echo '
<div class="wrapper">
	<div class="row no-gutters">
		<div class="col-lg-8 col-md-7 order-md-last d-flex align-items-stretch">
			<div class="contact-wrap w-100 p-md-5 p-4">
				<h3 class="mb-4">Get in touch</h3>
					<div id="form-message-warning" class="mb-4"></div>
						<div id="form-message-success" class="mb-4">
							Your message was sent, thank you!
						</div>
					<form method="POST" id="contactForm" name="contactForm" class="contactForm" novalidate="novalidate">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="label" for="name">Full Name</label>
						<input type="text" class="form-control" name="name" id="name" placeholder="Name">
					</div>
				</div>

		<div class="col-md-6">
			<div class="form-group">
				<label class="label" for="email">Email Address</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Email">
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<label class="label" for="subject">Subject</label>
				<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<label class="label" for="#">Message</label>
				<textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Message"></textarea>
			</div>
			</div>

		<div class="col-md-12">
			<div class="form-group">
				<input name="smt_contact" type="submit" value="Send Message" class="btn btn-primary
				   ">
				<div class="submitting"></div>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>

	<div class="col-lg-4 col-md-5 d-flex align-items-stretch">
		<div class="info-wrap bg-primary w-100 p-md-5 p-4">
			<h3>Let\'s get in touch</h3>
			<p class="mb-4">We\'re open for any suggestion or just to have a chat</p>

	<div class="dbox w-100 d-flex align-items-start">
		<div class="icon d-flex align-items-center justify-content-center">
			<span class="fa fa-map-marker"></span>
		</div>

			<div class="text pl-3">
				<p><span>Address: </span>'.env('ADDRESS').'</p>
			</div>
	</div>

	<div class="dbox w-100 d-flex align-items-center">
		<div class="icon d-flex align-items-center justify-content-center">
		<span class="fa fa-phone"></span>
		</div>
			<div class="text pl-3">
			<p><span>Phone:</span> <a href="tel://'.env('PHONE_NO').'">'.env('PHONE_NO').'</a></p>
		</div>
		</div>

	<div class="dbox w-100 d-flex align-items-center">
		<div class="icon d-flex align-items-center justify-content-center">
			<span class="fa fa-paper-plane"></span>
		</div>
			
		<div class="text pl-3">
		<p><span>Email:</span> <a href="mailto:'.env('EMAIL').'">'.env('EMAIL').'</a></p>
		</div>
	</div>

	<div class="dbox w-100 d-flex align-items-center">
		<div class="icon d-flex align-items-center justify-content-center">
			<span class="fa fa-globe"></span>
		</div>

	<div class="text pl-3">
		<p><span>Website</span> <a href="'.$globals['site_url'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
	</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script>

$(document).ready(function(){
	$("#contactForm").submit(function(e){
		var f_name, email, msg, subject;
		var error_list = [];
		$("#error").html("");
		f_name = $("#name").val().trim();
		email = $("#email").val().trim();
		subject = $("#subject").val().trim();
		msg = $("#message").val().trim();
		if(!f_name){
			error_list.push("Full Name is required");
		}
		if(!email){
			error_list.push("email is required");
		}
		if(!subject){
			error_list.push("subject is required");
		}
		if(!msg){
			error_list.push("Message is required");
		}
		if(!isEmail(email)){
			error_list.push("Email is not valid");
		}

		var str = error_hand_with_dismiss(error_list);
	   	$("#error").html(str).show();
		if(error_list.length > 0){
			return false;
		}
	});
});
</script>
';



ss_footer();
}

?>	