<?php

function login_theme(){

global $globals, $theme, $error, $done, $forgot, $forgot_conf;

	ss_header(STANDSKILLS.' - Login');
echo '
<style>
.main-content{
	width: 50%;
	border-radius: 20px;
	box-shadow: 0 5px 5px rgba(0,0,0,.4);
	margin: 5em auto;
	display: flex;
}
.company__info{
	background-color: #008080;
	border-top-left-radius: 20px;
	border-bottom-left-radius: 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	color: #fff;
}
.fa-android{
	font-size:3em;
}
@media screen and (max-width: 640px) {
	.main-content{width: 90%;}
	.company__info{
		display: none;
	}
	.login_form{
		border-top-left-radius:20px;
		border-bottom-left-radius:20px;
	}
}
@media screen and (min-width: 642px) and (max-width:800px){
	.main-content{width: 70%;}
}
.row > h2{
	color:#008080;
}
.login_form{
	background-color: #fff;
	border-top-right-radius:20px;
	border-bottom-right-radius:20px;
	border-top:1px solid #ccc;
	border-right:1px solid #ccc;
}
form{
	padding: 0 2em;
}
.form__input{
	width: 100%;
	border:0px solid transparent;
	border-radius: 0;
	border-bottom: 1px solid #aaa;
	padding: 1em .5em .5em;
	padding-left: 2em;
	outline:none;
	margin:1.5em auto;
	transition: all .5s ease;
}
.form__input:focus{
	border-bottom-color: #008080;
	box-shadow: 0 0 5px rgba(0,80,80,.4); 
	border-radius: 4px;
}
.btn{
	transition: all .5s ease;
	width: 70%;
	border-radius: 30px;
	color:#008080;
	font-weight: 600;
	background-color: #fff;
	border: 1px solid #008080;
	margin-top: 1.5em;
	margin-bottom: 1em;
}
.btn:hover, .btn:focus{
	background-color: #008080;
	color:#fff;
}
</style>
';

echo '
<div class="row">
	<ul class="col-sm-12 alert alert-danger" style="display:'.(!empty($error) ? '': 'none').'" role="alert" id="error">
		'.error_handle($error).'
	</ul>';
	if(!empty($done)){

		echo '<ul class="col-sm-12 alert alert-success" style="display:'.(!empty($done) ? '': 'none').'" role="alert" id="done">
		<li>'.$done['msg'].'</li>
	</ul>';
	}
	
echo '</div>';

if(!empty($forgot)){

echo '
<div class="row justify-content-center">
<div class="col-sm-6">
	<form method="post">
		<h1>Forgot Password</h1>
		<div class="form-group row">
	    	<label for="email" class="col-sm-4">Email address</label>
	    	<input type="email" class="form-control col-sm-8" id="email" name="email">
	  </div>
	  <input type="submit" name="forgotpass" class="btn btn-primary" value="Send Login Link">
  </form>
</div>
</div>
';

}else if(!empty($forgot_conf)){
	// var_dump($forgot_conf);exit;
	if(!empty($error) || !isset($forgot_conf['email'])){
		die();
	}

	echo '
<div class="row justify-content-center">
<div class="col-sm-6">
	<form method="post">
		<h1>Reset Password</h1>
		<input type="hidden" name="email" value="'.$forgot_conf['email'].'">
		<div class="form-group row">
	    	<label for="password" class="col-sm-4">Password</label>
	    	<input type="password" class="form-control col-sm-8" id="password" name="password">
	  </div>
	  <div class="form-group row">
	    	<label for="password" class="col-sm-4">Confirm Password</label>
	    	<input type="password" class="form-control col-sm-8" id="cnf_password" name="cnf_password">
	  </div>
	  <input type="submit" name="forgot_conf" class="btn btn-primary" value="Reset password">
  </form>
</div>
</div>
';	

}else{
	echo '
<div class="row main-content bg-success text-center">
	<div class="col-md-4 text-center company__info" onclick="window.location.href=\''.$globals['site_url'].'\'" style="cursor:pointer">
		<span class="company__logo" style="color: Mediumslateblue;"><h2><span class="fa fa-android"></span></h2></span>
		<h4 class="company_title">STANDSKILLS</h4>
	</div>
	<div class="col-md-8 col-xs-12 col-sm-12 login_form ">
		<div class="container-fluid">
			<div class="row">
				<h2>Login</h2>
			</div>
			<div class="row">
				<form control="" method="post" class="form-group">
					<div class="row">
						<input type="email" name="email" id="email" class="form__input" placeholder="Email">
					</div>
					<div class="row">
						<!-- <span class="fa fa-lock"></span> -->
						<input type="password" name="password" id="password" class="form__input" placeholder="Password">
					</div>
					<!--<div class="row">
						<input type="checkbox" name="remember_me" id="remember_me" class="">
						<label for="remember_me">Remember Me!</label>
					</div>-->
					<div class="row">
						<input type="submit" value="Submit" name="login" class="btn">
					</div>
				</form>
			</div>
			<div class="row">
				<p>Don\'t have an account? <a href="'.$globals['site_url'].'?act=register">Register Here</a></p><br>
				<p>Forgot Password? <a href="'.$globals['site_url'].'?act=login&forgot=pass">Click Here</a></p>
			</div>
		</div>
	</div>
</div>

';


	echo '
<!-- <div class="row justify-content-center">
<div class="col-sm-6">
	<form method="post">
		<h1>Login</h1>
		<div class="form-group row">
	    	<label for="email" class="col-sm-4">Email address</label>
	    	<input type="email" class="form-control col-sm-8" id="email" name="email">
	  </div>
	  <div class="form-group row">
	    	<label for="password" class="col-sm-4">Password</label>
	    	<input type="password" class="form-control col-sm-8" id="password" name="password">
	  </div>
	  <input type="submit" name="login" class="btn btn-primary" value="Login">? Create new Account <a href="'.$globals['site_url'].'?act=register">here</a><br>
	  Forgot Password <a href="'.$globals['site_url'].'?act=login&forgot=pass">Click Here</a>
  </form>
</div>
</div> -->
';
}

	ss_footer();

}