<?php

function payment_theme(){
	global $globals, $theme, $done, $error, $MYCONN, $data, $pay_details;

ss_header(env("APP_NAME", "standskills").' - Payment');
ss_navigation();

error_handle_with_dismiss($error);

done_handle_with_dismiss($done);

	$str = '';

	if(!empty($data['status']) && $data['status'] != 'Failed'){
		$str .= '
		<div class="row">
			<div class="col-sm-12">
				<div class="alert alert-success" role="alert">
				  <h4 class="alert-heading">Event is registered for your </h4>
				  <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
				  <hr>
				  <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
				  <a href="#" id="pay_details"> Click Here to View your Payment details</a>
				</div>
			</div>
		</div>
		<dl class="row pay_details" style="display:none">';

			foreach ($pay_details as $key => $value) {
				$str .= '<dt class="col-sm-6">'.$key.'</dt>
					<dd class="col-sm-6">'.$value.'</dd>';
			}

		$str .='
		</dl>';

	}elseif($data['status'] == 'Failed'){
		$str .= '<div class="row">
		<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Ohh payment is failed</h4>
		  <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
		  <hr>
		  <p class="mb-0">if Money is deducted from your bank your bank it will be refunded in 4-5 days of businessday.</p>
		  <a href="#"> Click here to view your paid price </a>
		</div></div>';
	}

echo $str;
echo '
<script>
$(document).ready(function(){

	$("#pay_details").click(function(){
		$(".pay_details").toggle("slow", "swing")
	})
});
</script>
';
ss_footer();
}