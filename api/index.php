<?php

include_once(dirname(dirname(__FILE__)).'/globals.php');
// var_dump($globals);exit;
include_once($globals['index'].'/functions.php');
include_once($globals['index'].'/modals/Database.php');

$act = optGET('action');
$debug = optGET('debug');
if(!empty($debug)){
	function get_error(){
		$response['debug'] = error_get_last();
		if($debug == 'died'){
			echo json_encode($response);exit;
		}
	}
	register_shutdown_function('get_error');
}

switch($act){
	case 'shayri':
		include_once($globals['index'].'/modals/Shayri.php');
		include_once('actions/shayri.php');
		break;

}
