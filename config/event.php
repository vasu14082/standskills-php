<?php

function get_event_p_type(){
	return [
		'audience',
		'Performer',
	];
}

function get_event_cat_type(){
	return [
		'poet',
		'Shayar',
		'Singer',
		'Stand Up Comedian',
		'Rapper',
		'Storyteller',
	];
}