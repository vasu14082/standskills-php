<?php
global $globals, $done, $error;
include_once(dirname(dirname(__FILE__)).'/globals.php');
// var_dump($globals);exit;
include_once($globals['index'].'/functions.php');
// var_dump(getenvs());exit;
include_once($globals['index'].'/modals/Database.php');
include_once($globals['index'].'/modals/migrations.php');
// var_dump($migrations, $argv, $argc);exit;
if($argc <=1){
	echo "please enter command".PHP_EOL;exit;
}

try{

	$db = new Database();
	$conn = $db->getConnection();
	// var_dump($error);exit;
	$sql = "CREATE TABLE IF NOT EXISTS migrations (
id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30) NOT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)";

// var_dump($conn);exit;
  $conn->exec($sql);

  $stmt = $conn->prepare("SELECT id, name FROM migrations");
  $stmt->execute();

  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $result = $stmt->fetchAll();

  $name_in_migrations = [];
  foreach ($result as $key => $value) {
  	$name_in_migrations[] = $value['name'];
  }
  
  // var_dump($name_in_migrations);exit;

  if(trim($argv[1]) == 'up'){

  	$act = trim($argv[1]);
  	foreach ($migrations as $key => $value) {

  		if(in_array($key, $name_in_migrations)){
  			continue;
  		}

  		$msql = 'INSERT INTO migrations(name) value (:m_name);';
  		// var_dump($value[$act]['action']);exit;
  		$taction = $value[$act]['action'];
  		if($taction == 'create'){
  			$q = create_table_query($value[$act]['table_name'], $value[$act]['schema']);
  			$stmt = $conn->prepare($msql);
  			$stmt->bindParam(':m_name', $key);
  			$stmt->execute();
  			$conn->exec($q);
  		}
  	}

  }
  // var_dump($result);exit;

}catch(Exception $e){

	$error['cli_migrate_error'] = $e->getMessage();
	echo implode(PHP_EOL,$error);
	die($e->getMessage());
	
}

echo "Migrated...".PHP_EOL;exit;