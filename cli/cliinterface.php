<?php
global $globals;
include_once(dirname(dirname(__FILE__)).'/globals.php');
include_once($globals['index'].'/functions.php');
include_once($globals['index'].'/main/connection.php');

$MYCONN = new MysqlConnect();
$MYCONN->create_database();

$action = [
	'drop_event_cat_table',
	'create_event_cat_table',
	'create_event_cat',
	'drop_event_p_type_table',
	'create_event_p_type_table',
	'create_event_p_type',
];

foreach ($action as $value) {
	call_user_func_array(array($MYCONN, $value), array());
}

$MYCONN->close_conn();